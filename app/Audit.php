<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $fillable = [
        'heading','description'
    ];

    public function questions(){
        return $this->hasMany(AuditQuestion::class);
    }

    public function answers() {
        return $this->hasMany(AuditAnswer::class);
    }
}
