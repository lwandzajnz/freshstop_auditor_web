<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditAnswer extends Model
{
    protected $fillable = [
         'field','value','site_audit_id','audit_id','question_id', 'user_id', 'site_id'
    ];
    
}
