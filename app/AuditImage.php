<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditImage extends Model
{
    protected $fillable = [
         'site_audit_id','question_id', 'image_type', 'image_path'
    ];
    
}
