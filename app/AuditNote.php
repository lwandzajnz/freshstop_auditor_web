<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditNote extends Model
{
    protected $fillable = [
       'site_audit_id', 'question_id', 'note', 'user_id', 'site_id'
    ];
    
}
