<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditQuestion extends Model
{
    protected $fillable = [
        'audit_id','question_number','question','question_name','question_type',
        'question_options','question_required','question_points','question_compliance'
    ];
    
}
