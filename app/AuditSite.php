<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditSite extends Model
{
  use SoftDeletes;
  
  protected $fillable = [
      'status','latitude','longitude'
  ];

  public function audit(){
      return $this->belongsTo(Audit::class);
  }

  public function site() {
      return $this->belongsTo(Sites::class);
  }

  public function questions(){
    // return $this->hasManyThrough(AuditQuestion::class, Audit::class);
    return $this->hasMany(AuditQuestion::class,'audit_id','audit_id');
  }

  public function answers() {
    return $this->hasMany(AuditAnswer::class,'audit_id');
  }

  public function stats() {
    return $this->hasOne(AuditStat::class);
  }
  
  public function user(){
    return $this->belongsTo(User::class);
  }

  public function scopeNotCompleted($request){
    return $request->where('status','!=',1);
  }

  public function scopeCompleted($request){
    return $request->where('status',1);
  }
  
}
