<?php

namespace App;

use App\Models\AppModels\Category;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['categories_id', 'file_name', 'file_type', 'document_path'];
    
    public function category() {
        return $this->belongsTo(Category::class,'categories_id','id');
    }
    
}
