<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Audit;
use App\AuditQuestion;
use App\Sites;
use App\AuditSite;
use App\User;
use DB;

use App\Models\Core\Setting;
use Illuminate\Http\Request;

use Scriptunited\Metroskin\Facades\Sitehelper as SiteHelpers;

class AuditController extends Controller
{
    public function __construct(Audit $audits, Setting $setting)
    {
        $this->Audit = $audits;
        $this->myVarsetting = new SiteSettingController($setting);
        $this->Setting = $setting;
    }

    public function dashboard(Request $request)
    {
        $title = ['pageTitle' => 'Dashboard'];
        $data['organisation'] = strtolower(str_replace(' ','',$request->organisation));
        // $sites = []; //Sites::with('user')->has('user')->get();
        $sites = AuditSite::with('user')->has('user')->get();
        
        $retailers = [];
        $current = [];
        $new = [];

        $count = 0;
        foreach ($sites as $site) {
            if ($data['organisation'] == 'freshstop') {

                $retailers[$site->area][] = $site;
                
            } elseif ($data['organisation'] == 'foodlovers') {
                $retailers[$site->area][] = $site;
            }
            ksort($retailers[$site->area]);
        }
    
        foreach ($retailers as $key => $value) {        
            foreach($value as $val){
                $retail[$key][] = $val->user->id;
            }
            $data['current'][$key] = array_count_values($retail[$key]); 
        }

        $array =[];
        foreach($data['current'] as $ckey => $current){
            foreach($current as $k => $c){
                foreach ($retailers as $key => $value) {
                    foreach($value as $v){
                        if($k == $v->user->id){
                            $array[$ckey][$k] = (object) ['id' => $v->user->id, 'name' => $v->user->first_name, 'total' => $c];
                        }
                    }
                }
            }
            $data['new'][$ckey] = $array[$ckey];
        }
        // return $data['new'];
        $data['retailers'] = $retailers;
        $data['commonContent'] = $this->Setting->commonContent();

        return view('admin.audits.dashboard_'.$data['organisation'],$title)->with('result',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = ['pageTitle' => 'Audits'];
        $data['audits'] = Audit::where('status',1)->orderBy('id','desc')->paginate(15);
        
        $data['commonContent'] = $this->Setting->commonContent();

        return view('admin.audits.index',$title)->with('result',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return SiteHelpers::userCan($access,$this);
        $title = ['pageTitle' => 'Audits'];
        $data['questions'] = [];
        $data['commonContent'] = $this->Setting->commonContent();
        // $data['js']['be_html'] = asset('metro/plugins/beautify/beautify-html.js');
		// $data['js']['zencode'] = asset('metro/plugins/jQuery-ZenCoding.js');
		// $data['js']['HTML']    = asset('metro/plugins/HTML.js');
		// $data['js']['pb']      = asset('metro/plugins/powerbuilder/form.js');
        // $data['js']['jqui']    = asset('metro/plugins/jquery-ui.min.js');

        $data['js']['be_html'] = 'metro/plugins/beautify/beautify-html.js';
		$data['js']['zencode'] = 'metro/plugins/jQuery-ZenCoding.js';
		$data['js']['HTML']    = 'metro/plugins/HTML.js';
		$data['js']['pb']      = 'metro/plugins/powerbuilder/form.js';
        $data['js']['jqui']    = 'metro/plugins/jquery-ui.min.js';

        // return $data;
        return view('admin.audits.create_dragdrop')->with('result',$data);
        // return view('admin.audits.create')->with('result',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $inputs = $request->all();
        return $inputs;
        unset($inputs['_token']);
        foreach ($inputs as $key => $value) {
            \DB::table('audit_answers')->insert(['audit_id' => $request->id, 'question_id' => 0, 'field' => $key, 'value' => $value]);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Audit  $audit
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Audit $audit)
    {
        $title = ['pageTitle' => 'Audits'];
        
        $data['js']['be_html'] = 'metro/plugins/beautify/beautify-html.js';
		$data['js']['zencode'] = 'metro/plugins/jQuery-ZenCoding.js';
		$data['js']['HTML']    = 'metro/plugins/HTML.js';
		$data['js']['pb']      = 'metro/plugins/powerbuilder/form.js';
        $data['js']['jqui']    = 'metro/plugins/jquery-ui.min.js';

        $data['audit'] = $audit::find($request->id);
        $data['form_data'] = \DB::table('audit_questions2')->where('audit_id',$request->id)->get();
        // $data['form_data'] = \DB::table('audit_questions')->where('audit_id',$request->id)->get();
        $data['answers'] = \DB::table('audit_answers')->where('audit_id',$request->id)->get();
        
        $data['commonContent'] = $this->Setting->commonContent();
        return view('admin.audits.show2')->with('result',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Audit  $audit
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Audit $audit)
    {
        $title = ['pageTitle' => 'Audits'];
        
        $data['js']['be_html'] = 'metro/plugins/beautify/beautify-html.js';
		$data['js']['zencode'] = 'metro/plugins/jQuery-ZenCoding.js';
		$data['js']['HTML']    = 'metro/plugins/HTML.js';
		$data['js']['pb']      = 'metro/plugins/powerbuilder/form.js';
        $data['js']['jqui']    = 'metro/plugins/jquery-ui.min.js';

        $data['audit'] = $audit::find($request->id);
        $data['form_data'] = \DB::table('audit_questions2')->where('audit_id',$request->id)->get();
        $data['commonContent'] = $this->Setting->commonContent();

        // return $data;

        return view('admin.audits.edit_dragdrop')->with('result',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Audit  $audit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Audit $audit)
    {
        $audit->status = ($audit->status == 1) ? 0:1;
        $audit->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Audit  $audit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Audit $audit)
    {
        //
    }
    
     public function user_audits(Request $request)
    {
        $title = ['pageTitle' => 'Audits'];
        
        $data['commonContent'] = $this->Setting->commonContent();

        $data['audits'] = DB::select("SELECT audit_sites.id, audit_sites.site_id, audits.heading, audit_sites.document, audit_sites.status, audit_sites.created_at, audit_sites.updated_at, audit_sites.deleted_at, sites.site_name 
        FROM `audit_sites`
        JOIN audits ON audits.id = audit_sites.audit_id
        JOIN sites on sites.id = audit_sites.site_id 
        WHERE audit_sites.user_id = $request->id
        ORDER BY audit_sites.updated_at DESC
        ");

        if(!$data['audits']){
            return redirect()->back();
        }
        // return $data;
        return view('admin.admins.audits')->with('result',$data);
    }

    public function user_audit_log(Request $request) {

        $log = DB::select("SELECT * from audit_logs where audit_site_id = $request->id");

        $data = "<table border='1'>
                    <tr>
                        <td>Method</td>
                        <td>Latitude</td>
                        <td>Longitude</td>
                        <td>Created</td>
                    </tr>";
                foreach ($log as $l) {
                    $data .= "<tr>
                        <td>{$l->method}</td>
                        <td>{$l->latitude}</td>
                        <td>{$l->longitude}</td>
                        <td>{$l->created_at}</td>
                    </tr>";
                }
        $data .=    "</table>";
        
        return view('admin.admins.audit_log', ['log' => $log]);
    }
    
    public function sendAuditReport(Request $request) {
		$audit = AuditSite::find($request->audit_site_id);
		
		if ($audit->status == 1) {
			event(new AuditCompleted($request->audit_site_id));
		    return redirect()->back()->withSuccess('Audit sent');
		}
		
		return redirect()->back()->withErrors('Audit not sent');
	}
	
    public function answers(Request $request, Audit $audit) {
        $title = ['pageTitle' => 'Audits'];
        
        $data['js']['be_html'] = 'metro/plugins/beautify/beautify-html.js';
		$data['js']['zencode'] = 'metro/plugins/jQuery-ZenCoding.js';
		$data['js']['HTML']    = 'metro/plugins/HTML.js';
		$data['js']['pb']      = 'metro/plugins/powerbuilder/form.js';
        $data['js']['jqui']    = 'metro/plugins/jquery-ui.min.js';

        $data['audit'] = $audit::find($request->id);
        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$request->id)->get()->toArray();
        $data['answers'] = \DB::table('audit_answers')->where('audit_id',$request->id)->get();
        
        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        foreach ($data['questions'] as $question) {
            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    break;
                }
            }
            $data['data'][] = (array) $question;
        }
        
        foreach($data['data'] as $key => $points) {
            
            if ($points['question_type_two'] == 'header') {
                $pointer = true;
                $temp_pointer = $key;
            } 

            if ($points['question_type_two'] !== 'header') {
                $total_points += $points['question_points'];
                isset($points['answer']) ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                $pointer = true;
                $temp_pointer = $key;
            } 
            
            $points['total_points'] = $total_points;
            $points['valid_points'] = 0;
            $points['invalid_points'] = 0;
            
            $data['point_data'][] = $points;

            if ($pointer == true) {
               
                $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;
               
                $pointer = false;
                $total_points = 0;
                $valid_points = 0;
                $invalid_points = 0;
            }

        }
        // return $data['point_data'];

        $data['form_data'] = (object) $data['point_data'];

        $object = new \stdClass();
        foreach ($data['point_data'] as $key => $value) {
            $data['form_data']->{$key} = (object) $value;
        }
        


        $data['commonContent'] = $this->Setting->commonContent();

        return view('admin.audits.answers')->with('result',$data);
    }

    public function generate(Request $request, Audit $audit)
    {
        
        $questions = $request->all();
        $audit_heading = $request->get('audit_heading');
        unset($questions['_token']);
        unset($questions['audit_heading']);
        unset($questions['submit']);
        unset($questions['campaign_id']);
        
        $total = count($questions);   
		
		$data = array();
		for($i = 1; $i < 21; $i++){
			foreach($questions as $key => $value){
				$new_key = explode('_',$key);
				if($new_key[1] ==  $i){
					$data[$i][$key] = $value;
				}
			}
		}
		
		$i = 1;
		$required = '';
		$input = '';
		$op = '';
		$textinput = '';
        
        $audit->heading = $audit_heading;
        $audit->save();
        $audit_id = $audit->id;

		foreach ($data as $dat) {
        
            if(!empty($dat['question_'.$i])){
                AuditQuestion::insert([
                    'audit_id' => $audit_id,
                    'question_number' => $i, 
                    'question' => $dat['question_'.$i], 
                    'question_type' => $dat['type_'.$i], 
                    'question_options' => $dat['options_'.$i], 
                    'question_required' => $dat['required_'.$i]
                ]);
            }
            $i++;
        /*
			foreach ($dat as $d => $v) {
				
				switch ($d) {
					case 'question_'.$i : 
						 if($d == 'question_'.$i){
							 // echo 'yes';
							$textinput = $v; 
						 };
					break;
					case 'options_'.$i : 
						$options = explode(",",$v);
						foreach ($options as $option) {
							$op .= '<option value="'.$option.'">'.$option.'</option>';
						};
					break;
					case 'required_'.$i : 
						if ($v == 'yes') { $required = ' required '; }; 
					break;
					case 'type_'.$i : 
						if ($d == 'type_'.$i) {
							if (strtolower($v) == 'short text') {
								$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input>';
							}
							if (strtolower($v) == 'long text') {
								$input = '<textarea class="form-control" name="{names}" {required} >{textinput}</textarea>';
							}
							if (strtolower($v) == 'select box') {
								$input = '<select class="form-control" name="{names}" {required} >{options}</select>';
							}
						}; 
					break;
				}
            }
        */
        }
        return redirect('admin/audits');
    }

    public function generate_builder_(Request $request, Audit $audit)
    {
        // return $request->get('builder_coder');
        return $request->all();
        $audit_heading = $request->get('audit_heading');
        $audit->heading = $audit_heading;
        $audit->save();
        $audit_id = $audit->id;
        // $audit_id = 0;
        $form_data = $request->get('fields');
        $points = $request->get('points');
        $flag = $request->get('flag');

        $i = 1;
        foreach ($form_data as $key => $value) {
            $result = explode(":",$value);
            $field_name = $key;
            $caption = $result[0];
            $field_type = $result[1];
            
            AuditQuestion::insert([
                'audit_id' => $audit_id,
                'question_number' => $i, 
                'question' => $caption, 
                'question_name' => $field_name, 
                'question_type' => $field_type, 
                'question_options' => '', 
                'question_required' => '',
                'question_points' => isset($points[$key])?$points[$key]:'',
                'question_compliance' => isset($flag[$key])?$flag[$key]:''
            ]);
            // $data[] = [
            //     'audit_id' => $audit_id,
            //     'question_number' => $i, 
            //     'question' => $caption, 
            //     'question_name' => $field_name, 
            //     'question_type' => $field_type, 
            //     'question_options' => '', 
            //     'question_required' => '',
            //     'question_points' => isset($points[$key])?$points[$key]:'',
            //     'question_compliance' => isset($flag[$key])?$flag[$key]:''
            // ];
            $i++;
        }
        // return $data;
        // return redirect()->back();
    }

    public function generate_builder(Request $request, Audit $audit)
    {
        // return $request->all();
        $audit->heading = $request->get('audit_heading');
        $audit->audit_type = $request->get('audit_type');
        $audit->region = $request->get('region');
        $audit->status = $request->get('status');
        $audit->save();
        $audit_id = $audit->id;

        $form_data = $request->get('builder_coder');
        
        $count = 0;

        $array = [];
        $dom = new \DOMDocument;
        libxml_use_internal_errors(true);
        $dom->loadHTML($form_data);

        foreach ($dom->getElementsByTagName('input') as $node)
        {
            $array[$count]['type'] = $node->getAttribute('type');
            $array[$count]['type_two'] = $node->getAttribute('type_two');
            $array[$count]['name'] = $node->getAttribute('name');
            $array[$count]['value'] = $node->getAttribute('value');
            $array[$count]['class'] = $node->getAttribute('class');
            $array[$count]['caption'] = $node->getAttribute('caption');
            $array[$count]['points'] = $node->getAttribute('points');
            $array[$count]['flag'] = $node->getAttribute('flag');
            $count++;
        }

        // return $array;
        
        DB::table('audit_questions2')->insert(array('audit_id' => $audit_id, 'form_data' => $form_data));
        // foreach ($form_data as $key => $value) {
        //     echo $key . ' => ' . $value[$key];
        //     echo "<br/>";
        // }

        foreach ($array as $element) {
            DB::table('audit_questions')->insert([
                'audit_id' => $audit_id,
                'question_name' => $element['name'],
                'question' => $element['caption'],
                'question_type' => $element['type'],
                'question_type_two' => $element['type_two'],
                'question_options' => '',
                'question_required' => '',
                'question_number' => 0,
                'question_points' => $element['points'],
                'question_compliance' => $element['flag'],
            ]);
        }

    }

}
