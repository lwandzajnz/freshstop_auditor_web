<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\AuditQuestion;
use Illuminate\Http\Request;

class AuditQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AuditQuestion  $auditQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(AuditQuestion $auditQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AuditQuestion  $auditQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(AuditQuestion $auditQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AuditQuestion  $auditQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuditQuestion $auditQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AuditQuestion  $auditQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditQuestion $auditQuestion)
    {
        //
    }

    public function generate(AuditQuestion $auditQuestion)
    {
        return $request->all();
        $questions = $this->input->post();
		$campaign_id = $this->input->post('campaign_id');
        unset($questions['submit']);
        unset($questions['campaign_id']);
		// sort($questions);
     
        $total = count($questions);   
		
		$data = array();
		for($i = 1; $i < 21; $i++){
			foreach($questions as $key => $value){
				$new_key = explode('_',$key);
				if($new_key[1] ==  $i){
					$data[$i][$key] = $value;
				}
			}
		}
		
		$i = 1;
		$required = '';
		$input = '';
		$op = '';
		$textinput = '';
		
		foreach ($data as $dat) {
				$div = '';
				$this->db->insert('campaign_questions',array('campaign_id' => $campaign_id, 'question_number' => $i, 'question_question' => $dat['question_'.$i], 'question_type' => $dat['type_'.$i], 'question_options' => $dat['options_'.$i], 'question_required' => $dat['required_'.$i]));
			foreach ($dat as $d => $v) {
				
				switch ($d) {
					case 'question_'.$i : 
						 if($d == 'question_'.$i){
							 // echo 'yes';
							$textinput = $v; 
						 };
					break;
					case 'options_'.$i : 
						$options = explode(",",$v);
						foreach ($options as $option) {
							$op .= '<option value="'.$option.'">'.$option.'</option>';
						};
					break;
					case 'required_'.$i : 
						if ($v == 'yes') { $required = ' required '; }; 
					break;
					case 'type_'.$i : 
						if ($d == 'type_'.$i) {
							if (strtolower($v) == 'short text') {
								$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input>';
							}
							if (strtolower($v) == 'long text') {
								$input = '<textarea class="form-control" name="{names}" {required} >{textinput}</textarea>';
							}
							if (strtolower($v) == 'select box') {
								$input = '<select class="form-control" name="{names}" {required} >{options}</select>';
							}
						}; 
					break;
				}
			}
    }
}
