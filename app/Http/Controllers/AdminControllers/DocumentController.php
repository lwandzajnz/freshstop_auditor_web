<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Document;
use DB;
use App\Models\Core\Setting;
use App\Models\Core\Categories;
use App\Models\Core\Languages;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function __construct(Document $documents, Setting $setting, Categories $category, Languages $language)
    {   
        $this->language = $language;
        $this->category = $category;
        $this->Document = $documents;
        $this->myVarsetting = new SiteSettingController($setting);
        $this->Setting = $setting;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = array('pageTitle' => 'Documents');
        $result = array();
        $language_id = '1';
   
        $result['documents'] = Document::with('category')->paginate(20);
   
        $result['commonContent'] = $this->Setting->commonContent();
        $categories = $this->category->recursivecategories($request);

        $parent_id = array();
        $option = '<ul class="list-group list-group-root well">';

        foreach ($categories as $parents) {

            if (in_array($parents->id, $parent_id)) {
                $checked = 'checked';
            } else {
                $checked = '';
            }

            $option .= '<li href="#" class="list-group-item">
          <label style="width:100%">
            <input id="categories_' . $parents->id . '" ' . $checked . ' type="checkbox" class=" required_one categories sub_categories" name="categories[]" value="' . $parents->id . '">
          ' . $parents->categories_name . '
          </label></li>';

            if (isset($parents->childs)) {
                $option .= '<ul class="list-group">
          <li class="list-group-item">';
                $option .= $this->childcat($parents->childs, $parent_id);
                $option .= '</li></ul>';
            }
        }
        $option .= '</ul>';

        $result['categories'] = $option;

        
        $taxClass = DB::table('tax_class')->get();
        $result['taxClass'] = $taxClass;
        $result['languages'] = $this->myVarsetting->getLanguages();
        $result['units'] = $this->myVarsetting->getUnits();
        
        return view('admin.documents.index',$title)->with('result',$result);
    }

    public function categories(Request $request)
    {
        $title = array('pageTitle' => 'Documents');
        $result = array();
        $language_id = '1';
        $result['documents'] = Document::where('categories_id',$request->id)->paginate(20);
        
        $result['commonContent'] = $this->Setting->commonContent();
        $categories = $this->category->recursivecategories($request);

        $parent_id = array();
        $option = '<ul class="list-group list-group-root well">';

        foreach ($categories as $parents) {

            if (in_array($parents->id, $parent_id)) {
                $checked = 'checked';
            } else {
                $checked = '';
            }

            $option .= '<li href="#" class="list-group-item">
          <label style="width:100%">
            <input id="categories_' . $parents->id . '" ' . $checked . ' type="checkbox" class=" required_one categories sub_categories" name="categories[]" value="' . $parents->id . '">
          ' . $parents->categories_name . '
          </label></li>';

            if (isset($parents->childs)) {
                $option .= '<ul class="list-group">
          <li class="list-group-item">';
                $option .= $this->childcat($parents->childs, $parent_id);
                $option .= '</li></ul>';
            }
        }
        $option .= '</ul>';

        $result['categories'] = $option;

        $result['manufacturer'] = $this->manufacturer->getter($language_id);
        $taxClass = DB::table('tax_class')->get();
        $result['taxClass'] = $taxClass;
        $result['languages'] = $this->myVarsetting->getLanguages();
        $result['units'] = $this->myVarsetting->getUnits();
        
        return view('admin.documents.index',$title)->with('result',$result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Document $document)
    {
        $file = $request->file('file');
        $time = Carbon::now();
        if ($request->hasFile('file')){ 
            $extension = $file->getClientOriginalExtension();
            // Creating the directory, for example, if the date = 18/10/2017, the directory will be 2017/10/
            $directory = date_format($time, 'Y') . '/' . date_format($time, 'm');
            // Creating the file name: random string followed by the day, random number and the hour
            // $filename = str_random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . "." . $extension;
            $filename = $file->getClientOriginalName();
            // This is our upload main function, storing the image in the storage that named 'public'
            $upload_success = $file->storeAs($directory, $filename, 'local');
            
            $path = storage_path('app/' . $directory . '/' . $filename);
            
            $document->create([
                'categories_id' => $request->get('categories')[0],
                'admin_type_id' => 1,
                'file_name' => $filename,
                'file_type' => $extension,
                'document_path' => $path
            ]);

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Document $document)
    {
        
        $doc = $document->find($request->id);
        // return $doc;
        // $file = storage_path('app/'.$doc->document_path);
        // $file = \File::get($doc->document_path);
        // $google = 'https://docs.google.com/viewerng/viewer?url=' . $file;

        $content_type = mime_content_type($doc->document_path);

        // return response()->file($doc->document_path,[
        //     'Content-Type' => $content_type,
        //     'filename' => $doc->file_name, 
        //     'name' => $doc->file_name,
        // ]);
        
        return response()->download($doc->document_path, $doc->file_name, ['Content-Type' => $content_type]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        $document->delete();

        return redirect()->back();
    }
}
