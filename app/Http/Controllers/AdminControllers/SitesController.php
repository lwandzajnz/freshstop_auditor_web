<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Sites;
use App\Audit;
use App\Models\Core\Language;
use App\Models\Core\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class SitesController extends Controller
{

    public function __construct(Sites $sites)
    {
        $setting = new Setting();
        $this->Setting = $setting;
        $this->Sites = $sites->orderBy('site_name','ASC');
    }

    public function index() 
    {
      $title = ['pageTitle' => 'Sites'];

      $data['sites'] = $this->Sites->paginate(15);
      $data['commonContent'] = $this->Setting->commonContent();

      return view('admin.sites.index', $title)->with('result',$data);
    }
    
        public function add(Request $request){
      $title = ['pageTitle' => 'Sites'];
      $data['commonContent'] = $this->Setting->commonContent();

      return view('admin.sites.create', $title)->with('result',$data);
    }

    public function create(Request $request){
      Sites::create($request->only(['site_name', 'bc', 'bc_name','bc_email', 'retailer','area','address','landline','cell_no','email','status']));
      return redirect('/admin/sites');
    }

    public function edit(Request $request){
      $title = ['pageTitle' => 'Sites'];
      $data['commonContent'] = $this->Setting->commonContent();

      $data['site'] = Sites::findOrFail($request->id);
      
      return view('admin.sites.edit', $title)->with('result',$data);
    }

    public function update(Request $request){
      
      Sites::find($request->id)->update($request->only(['site_name', 'bc', 'bc_name','bc_email', 'retailer','area','address','landline','cell_no','email','status']));

      return redirect('/admin/sites');
    }

    public function destroy(Request $request){
      
      Sites::find($request->id)->delete();
      
      return redirect('/admin/sites');
    }
    
    public function show(Request $request) 
    {
      $title = ['pageTitle' => 'Sites'];

      $data['site'] = $this->Sites->find($request->id);
      $data['audits'] = Audit::paginate(15);
      $data['commonContent'] = $this->Setting->commonContent();

      return view('admin.sites.show', $title)->with('result',$data);
    }


}
