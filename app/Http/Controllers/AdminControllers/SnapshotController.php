<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Events\AuditCompleted;
use App\Audit;
use App\AuditSite;
use App\AuditStat;
use App\AuditQuestion;
use App\Sites;
use DB;
use Mail;


use App\Models\Core\Setting;
use Illuminate\Http\Request;

use Scriptunited\Metroskin\Facades\Sitehelper as SiteHelpers;

class SnapshotController extends Controller
{
    public function __construct(Audit $audits, Setting $setting)
    {
        $this->Audit = $audits;
        $this->myVarsetting = new SiteSettingController($setting);
        $this->Setting = $setting;
    }

    public function dashboard(Request $request){
        
        $sites = Sites::all();

        foreach ($sites as $site) {
            $stats = AuditSite::with(['site','user','stats','audit'])->where('site_id', $site->id)->where('status',1)->first();
            if($stats){
                $statistics = AuditStat::where('audit_site_id', $stats->id)->first();
                $data['retailers'][$site->area][$stats->user->id][] = (object) ['site' => $stats, 'stats' => $statistics];
            }
        }  
        
        // return $data;

        $data['commonContent'] = $this->Setting->commonContent();

        return view('admin.audits.dashboard_freshstop_snapshots')->with('result',$data);
    }
    
    public function dashboard1(Request $request){
        $sites = Sites::with('user')->has('user')->get();

        foreach ($sites as $site) {
            $stats = AuditSite::with('stats')->where('site_id',$site->id)->first();
            $data['retailers'][$site->area][$site->user->id][] = 
                (object) ['site' => $site, 'stats' => $stats];
        }
        // return $data['retailers'];
        $data['commonContent'] = $this->Setting->commonContent();

        return view('admin.audits.dashboard_freshstop_snapshots')->with('result',$data);
    }

    public function index(Request $request) {
        $title = ['pageTitle' => 'Audits'];
        
        $data['js']['be_html'] = 'metro/plugins/beautify/beautify-html.js';
        $data['js']['zencode'] = 'metro/plugins/jQuery-ZenCoding.js';
        $data['js']['HTML']    = 'metro/plugins/HTML.js';
        $data['js']['pb']      = 'metro/plugins/powerbuilder/form.js';
        $data['js']['jqui']    = 'metro/plugins/jquery-ui.min.js';

        $data['audit'] = AuditSite::with(['audit','site'])->where(['user_id'=>$request->user_id, 'site_id' => $request->site_id]);
        
        if(!$data['audit']->first()) return response()->json('no audit for this site');
        $data['audit'] = $data['audit']->first();
        
        
        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$data['audit']->audit_id)->get()->toArray();
        $data['answers'] = \DB::table('audit_answers')->where('site_audit_id',$data['audit']->id)->get();
        $data['images'] = \DB::table('audit_images')->where('site_audit_id',$data['audit']->id)->get();
        $data['notes'] = \DB::table('audit_notes')->where('site_audit_id', $data['audit']->id)->get();
        
        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        $total_headers = 0;
        $data['total_valid'] = 0;
        $data['total_invalid'] = 0;
        $data['total_points'] = 0;
        $data['total_inspection_score'] = 0;
        
        foreach ($data['questions'] as $question) {
                $question->answer = null;
                $question->answer2 = null;
            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    $question->answer2 = $answers->value2;
                    break;
                }
            }
            foreach ($data['images'] as $image) {
                if ($question->id == $image->question_id) {
                    $question->images[] = $image->image_path;
                }
            }
            foreach ($data['notes'] as $note) {
                if ($question->id == $note->question_id) {
                    $question->notes[] = $note;
                }
            }
            $data['data'][] = (array) $question;
        }
        
        foreach($data['data'] as $key => $points) {
            
            if ($points['question_type_two'] == 'header') {
                $pointer = true;
                $temp_pointer = $key;
            }

            if ($points['question_type_two'] !== 'header') {
                $total_points += $points['question_points'];
                (isset($points['answer']) && ($points['answer'] == 'YES')) ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                $pointer = true;
            } 
            
            $points['total_points'] = $total_points;
            $points['valid_points'] = 0;
            $points['invalid_points'] = 0;
            
            $data['point_data'][] = $points;

            if ($pointer) {
               
                $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;
                 
                $pointer = false;
                $total_points = 0;
                $valid_points = 0;
                $invalid_points = 0;
            }

        }
        unset($data['data']);
        unset($data['questions']);
        $data['form_data'] = (object) $data['point_data'];
       

        $object = new \stdClass();
        foreach ($data['point_data'] as $key => $value) {
          $data['form_data']->{$key} = (object) $value;
          if(!in_array($data['form_data']->{$key}->question_type_two,['signature','file']) && $data['form_data']->{$key}->question_type_two == 'header'){
            $data['total_inspection_score'] += ($data['form_data']->{$key}->valid_points / $data['form_data']->{$key}->total_points);
            $data['total_valid'] += $data['form_data']->{$key}->valid_points;
            $data['total_invalid'] += $data['form_data']->{$key}->invalid_points;
            $data['total_points'] += $data['form_data']->{$key}->total_points;
            $total_headers += 1;
          }
        }

        unset($data['point_data']);
       
        $data['total_inspection_score'] =  $data['total_inspection_score'] * $total_headers;

        $data['commonContent'] = $this->Setting->commonContent();
  
  /*
        $pdf = \PDF::loadView('admin.snapshots.pdf', ['result' => $data]);
        $audit_filename = "audit_reports/audit_{$data['audit']->id}_".date('Y-m-d').".pdf";
        $pdf->save($audit_filename);
    */    
        
        // $email = 'jamie@jnz.co.za';
        // $name = 'Jamie';
        // Mail::send("mail.audit_report", [], function ($mailer) use ($email, $name,$audit_filename) {
        //     $mailer->from('noreply@freshstop.co.za', 'Freshstop');
        //     $mailer->to($email, $name);
        //     $mailer->subject('Audit Report');
        //     $mailer->attach($audit_filename);
        // });

        
      
        
        return view('admin.snapshots.index')->with('result',$data);
    }
    
    public function report(Request $request) {
        $title = ['pageTitle' => 'Audits'];
        
    
        $audit = AuditSite::with(['audit','site'])->find($request->audit_site_id);
        
        if(count((array) $audit) < 1) return response()->json('no audit for this site');
        
        $data['audit'] = $audit;
        
        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$audit->audit_id)->get();
        $data['answers'] = \DB::table('audit_answers')->where('site_audit_id',$audit->id)->get();
        $data['images'] = \DB::table('audit_images')->where('site_audit_id',$audit->id)->get();
        $data['notes'] = \DB::table('audit_notes')->where('site_audit_id', $audit->id)->get();
        
        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        $total_headers = 0;
        $failed_items = 0;
        $data['failed_questions'] = [];

        $data['total_valid'] = 0;
        $data['total_invalid'] = 0;
        $data['total_invalid_items'] = 0;
        $data['total_points'] = 0;
        $data['total_inspection_score'] = 0;
        
        foreach ($data['questions'] as $question) {
                $question->answer = null;
                $question->answer2 = null;
            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    $question->answer2 = $answers->value2;
                    break;
                }
            }
            foreach ($data['images'] as $image) {
                if ($question->id == $image->question_id) {
                    $question->images[] = $image->image_path;
                }
            }
            foreach ($data['notes'] as $note) {
                if ($question->id == $note->question_id) {
                    $question->notes[] = $note;
                }
            }
            $data['data'][] = (array) $question;
        }
        $temp_pointer = 0;
        foreach($data['data'] as $key => $points) {
            
            if ($points['question_type_two'] == 'header') {
                $pointer = true;
                $temp_pointer = $key;
            }

            if ($points['question_type_two'] !== 'header') {
                $total_points += $points['question_points'];
                (isset($points['answer']) && ($points['answer'] == 'YES')) ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                $pointer = true;
            } 
            
            $points['total_points'] = $total_points;
            $points['valid_points'] = 0;
            $points['invalid_points'] = 0;
            
            if($points['answer'] == 'NO'){
                $failed_items += 1;
                $data['failed_questions'][] = ['id' => $points['id'], 'question' => $points['question']];
            }

            $data['point_data'][] = $points;
            if(!in_array($request->report_type, ['contact','snapshot','food'])){
                if ($pointer) {
                   
                    $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                    $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                    $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;
                     
                    $pointer = false;
                    $total_points = 0;
                    $valid_points = 0;
                    $invalid_points = 0;
                }
            }

        }
        unset($data['data']);
        unset($data['questions']);

        $data['failed_items'] = $failed_items;

        $data['form_data'] = (object) $data['point_data'];
        
        $object = new \stdClass();
        foreach ($data['point_data'] as $key => $value) {
          $data['form_data']->{$key} = (object) $value;
          if(!in_array($data['form_data']->{$key}->question_type_two,['signature','file']) && $data['form_data']->{$key}->question_type_two == 'header'){
              
           	if($data['form_data']->{$key}->total_points > 0){
                $data['total_inspection_score'] += ($data['form_data']->{$key}->valid_points / $data['form_data']->{$key}->total_points);
            }else{
                    $data['total_inspection_score'] += 0;
            }
            $data['total_valid'] += $data['form_data']->{$key}->valid_points;
            $data['total_invalid'] += $data['form_data']->{$key}->invalid_points;
            $data['total_invalid_items'] += ($data['form_data']->{$key}->invalid_points > 0)? 1:0;
            $data['total_points'] += $data['form_data']->{$key}->total_points;
        
            $total_headers += 1;
          }
        }

        unset($data['point_data']);
       
        if(!in_array($request->report_type, ['contact','snapshot','food'])){
            $data['total_inspection_score'] =  ($data['total_valid'] / $data['total_points']) * 100;
        }
        
        $data['commonContent'] = $this->Setting->commonContent();
  
        return view('admin.snapshots.index')->with('result',$data);
    }
      
    public function download($site_audit_id) {

        $audit = AuditSite::find($site_audit_id);

        $file= public_path($audit->document);

        $headers = array(
                  'Content-Type: application/pdf',
                );
    
        return response()->download($file, "audit_{$site_audit_id}_".date('Y-m-d').".pdf", $headers);
    }

    
    
    
        
    
    public function update_report()
    {

        $site_audit_id = 430;
        $audit_info = AuditSite::with(['audit','user'])->find($site_audit_id);

        $data['audit'] = AuditSite::with(['audit','site'])->find($site_audit_id);

        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$audit_info->audit_id)->get()->toArray();
        $data['answers'] = \DB::table('audit_answers')->where('site_audit_id',$audit_info->id)->get();
        $data['images'] = \DB::table('audit_images')->where('site_audit_id',$data['audit']->id)->get();
        $data['notes'] = \DB::table('audit_notes')->where('site_audit_id', $data['audit']->id)->get();
        
        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        $total_headers = 0;
        $data['total_valid'] = 0;
        $data['total_invalid'] = 0;
        $data['total_points'] = 0;
        $data['total_inspection_score'] = 0;
        foreach ($data['questions'] as $question) {
            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    $question->answer2 = $answers->value2;
                    break;
                }
                foreach ($data['images'] as $image) {
                    if ($question->id == $image->question_id) {
                        $question->images[] = $image->image_path;
                    }
                }
                foreach ($data['notes'] as $note) {
                    if ($question->id == $note->question_id) {
                        $question->notes[] = $note;
                    }
                }
            }
            $data['data'][] = (array) $question;
        }
        
        if($audit_info->audit->audit_type != 'contact'){
            foreach($data['data'] as $key => $points) {
                
                if ($points['question_type_two'] == 'header') {
                    $pointer = true;
                    $temp_pointer = $key;
                }
    
                if ($points['question_type_two'] !== 'header') {
                    $total_points += $points['question_points'];
                    ($points['answer'] == 'YES') ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                    $pointer = true;
                } 
                
                $points['total_points'] = $total_points;
                $points['valid_points'] = 0;
                $points['invalid_points'] = 0;
                
                $data['point_data'][] = $points;
    
                if ($pointer) {
                   
                    $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                    $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                    $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;
                     
                    $pointer = false;
                    $total_points = 0;
                    $valid_points = 0;
                    $invalid_points = 0;
                }
    
            }
            unset($data['data']);
            unset($data['answers']);
            unset($data['questions']);
            $data['form_data'] = (object) $data['point_data'];
           
          
            $object = new \stdClass();
            foreach ($data['point_data'] as $key => $value) {
              $data['form_data']->{$key} = (object) $value;
              if(!in_array($data['form_data']->{$key}->question_type_two,['signature','file']) && $data['form_data']->{$key}->question_type_two == 'header'){
                  if($data['form_data']->{$key}->total_points > 0){
                    $data['total_inspection_score'] += ($data['form_data']->{$key}->valid_points / $data['form_data']->{$key}->total_points);
                  }else{
                      $data['total_inspection_score'] += 0;
                  }
                $data['total_valid'] += $data['form_data']->{$key}->valid_points;
                $data['total_invalid'] += $data['form_data']->{$key}->invalid_points;
                $data['total_points'] += $data['form_data']->{$key}->total_points;
                $total_headers += 1;
              }
            }
            // unset($data['form_data']);
            unset($data['point_data']);
        
        }
        $data['total_inspection_score'] =  $data['total_inspection_score'] * $total_headers;
        
        $pdf = \PDF::loadView('admin.snapshots.pdf', ['result' => $data]);
        $audit_filename = "audit_reports/audit_{$audit_info->id}_".date('Y-m-dH:i:s')."_RR.pdf";
        $pdf->save($audit_filename);
        
    }
    
    public function view_report(Request $request)
    {
        $site_audit_id = $request->id;
        $audit_info = AuditSite::with(['audit','user'])->find($site_audit_id);

        $data['audit'] = AuditSite::with(['audit','site'])->find($site_audit_id);

        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$audit_info->audit_id)->get()->toArray();
        $data['answers'] = \DB::table('audit_answers')->where('site_audit_id',$audit_info->id)->get();
        $data['images'] = \DB::table('audit_images')->where('site_audit_id',$data['audit']->id)->get();
        $data['notes'] = \DB::table('audit_notes')->where('site_audit_id', $data['audit']->id)->get();
        
        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        $total_headers = 0;
        $data['total_valid'] = 0;
        $data['total_invalid'] = 0;
        $data['total_points'] = 0;
        $data['total_inspection_score'] = 0;
        
        foreach ($data['questions'] as $question) {
              $question->answer = null;
              $question->answer2 = null;
                
            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    $question->answer2 = $answers->value2;
                    break;
                }
            }
            foreach ($data['images'] as $image) {
                if ($question->id == $image->question_id) {
                    $question->images[] = $image->image_path;
                    // break;
                    // echo "{$question->id} {$image->question_id} {$image->image_path} \r\n";
                }
            }
            foreach ($data['notes'] as $note) {
                if ($question->id == $note->question_id) {
                    $question->notes[] = $note;
                }
            }
            $data['data'][] = (array) $question;
        }
        
    // return [];
    //   return $data['data'];
            foreach($data['data'] as $key => $points) {
                
                if ($points['question_type_two'] == 'header') {
                    $pointer = true;
                    $temp_pointer = $key;
                }
    
                if ($points['question_type_two'] !== 'header') {
                    $total_points += $points['question_points'];
                    ($points['answer'] == 'YES') ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                    $pointer = true;
                } 
                
                $points['total_points'] = $total_points;
                $points['valid_points'] = 0;
                $points['invalid_points'] = 0;
                
                $data['point_data'][] = $points;
               if($audit_info->audit->audit_type != 'contact'){
                if ($pointer) {
                   
                    $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                    $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                    $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;
                     
                    $pointer = false;
                    $total_points = 0;
                    $valid_points = 0;
                    $invalid_points = 0;
                }
               }
    
            }
            unset($data['data']);
            unset($data['answers']);
            unset($data['questions']);
            $data['form_data'] = (object) $data['point_data'];
           
           
            $object = new \stdClass();
            foreach ($data['point_data'] as $key => $value) {
              $data['form_data']->{$key} = (object) $value;
              if(!in_array($data['form_data']->{$key}->question_type_two,['signature','file']) && $data['form_data']->{$key}->question_type_two == 'header'){
                  if($data['form_data']->{$key}->total_points > 0){
                    $data['total_inspection_score'] += ($data['form_data']->{$key}->valid_points / $data['form_data']->{$key}->total_points);
                  }else{
                      $data['total_inspection_score'] += 0;
                  }
                $data['total_valid'] += $data['form_data']->{$key}->valid_points;
                $data['total_invalid'] += $data['form_data']->{$key}->invalid_points;
                $data['total_points'] += $data['form_data']->{$key}->total_points;
                $total_headers += 1;
              }
            }
            
            unset($data['point_data']);
        
        
        $data['total_inspection_score'] =  $data['total_inspection_score'] * $total_headers;
        
        return view('admin.snapshots.pdf2')->with('result',$data);
        
    }
    
    
    
    public function view_pdf(){
        $site_audit_id = 855;
        $audit_info = AuditSite::with(['audit','user'])->find($site_audit_id);

        $data['audit'] = AuditSite::with(['audit','site'])->find($site_audit_id);

        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$audit_info->audit_id)->get()->toArray();
        $data['answers'] = \DB::table('audit_answers')->where('site_audit_id',$audit_info->id)->get();
        $data['images'] = \DB::table('audit_images')->where('site_audit_id',$data['audit']->id)->get();
        $data['notes'] = \DB::table('audit_notes')->where('site_audit_id', $data['audit']->id)->get();
        
        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        $total_headers = 0;
        $failed_items = 0;
        $data['total_valid'] = 0;
        $data['total_invalid'] = 0;
        $data['total_points'] = 0;
        $data['total_inspection_score'] = 0;
        foreach ($data['questions'] as $question) {
              $question->answer = null;
              $question->answer2 = null;
              
            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    $question->answer2 = $answers->value2;
                    break;
                }
            }
            foreach ($data['images'] as $image) {
                if ($question->id == $image->question_id) {
                    $question->images[] = $image->image_path;
                }
            }
            foreach ($data['notes'] as $note) {
                if ($question->id == $note->question_id) {
                    $question->notes[] = $note;
                }
            }
           
            $data['data'][] = (array) $question;
        }
        
        $temp_pointer = 0;
        foreach($data['data'] as $key => $points) {
            
            if ($points['question_type_two'] == 'header') {
                $pointer = true;
                $temp_pointer = $key;
            }

            if ($points['question_type_two'] !== 'header') {
                $total_points += $points['question_points'];
                ($points['answer'] == 'YES') ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                $failed_items += ($points['answer'] == 'NO')?1:0;
                $pointer = true;
            } 
            
            $points['total_points'] = $total_points;
            $points['valid_points'] = 0;
            $points['invalid_points'] = 0;
            
            $data['point_data'][] = $points;
            
            if(!in_array($audit_info->audit->audit_type,['contact','snapshot'])){
                if ($pointer) {
                   
                    $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                    $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                    $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;
                     
                    $pointer = false;
                    $total_points = 0;
                    $valid_points = 0;
                    $invalid_points = 0;
                }
            }
        }
        unset($data['data']);
        unset($data['answers']);
        unset($data['questions']);
        
        $data['failed_items'] = $failed_items;
        
        $data['form_data'] = (object) $data['point_data'];
      
        $object = new \stdClass();
        foreach ($data['point_data'] as $key => $value) {
          $data['form_data']->{$key} = (object) $value;
          if(!in_array($data['form_data']->{$key}->question_type_two,['signature','file']) && $data['form_data']->{$key}->question_type_two == 'header'){
              if($data['form_data']->{$key}->total_points > 0){
                $data['total_inspection_score'] += ($data['form_data']->{$key}->valid_points / $data['form_data']->{$key}->total_points);
              }else{
                  $data['total_inspection_score'] += 0;
              }
            $data['total_valid'] += $data['form_data']->{$key}->valid_points;
            $data['total_invalid'] += $data['form_data']->{$key}->invalid_points;
            $data['total_points'] += $data['form_data']->{$key}->total_points;
            $total_headers += 1;
          }
        }
        
        unset($data['point_data']);
        
        // $data['total_inspection_score'] =  $data['total_inspection_score'] * $total_headers;
        if(!in_array($audit_info->audit->audit_type,['contact','snapshot'])){
            $data['total_inspection_score'] = ($data['total_valid'] / $data['total_points']) * 100;
        }
        
         $pdf = \PDF::loadView('admin.snapshots.pdf', ['result' => $data])->stream('filename.pdf');
         return $pdf;
        // $audit_filename = "audit_reports/audit_{$audit_info->id}_".date('YmdHis').".pdf";
        
        //  return view('admin.snapshots.pdf')->with('result',$data);
         
    }
    
    
}
