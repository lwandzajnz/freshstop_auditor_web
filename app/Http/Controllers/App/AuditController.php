<?php
namespace App\Http\Controllers\App;

//validator is builtin class in laravel
use Validator;
use DB;
use DateTime;
use Hash;
use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon;
use App\Models\AppModels\News;
use App\Audit;
use App\AuditQuestion;
use App\AuditAnswer;
use App\AuditNote;
use App\AuditAction;
use App\AuditImage;
use App\AuditSite;
use App\AuditLog;
use Mail;

use App\Events\AuditCompleted;

class AuditController extends Controller
{

	//allnewscategories
	public function allnewscategories(Request $request){
    $categoryResponse = News::allnewscategories($request);
		print $categoryResponse;
	}


	//getallnews
	public function getallnews(Request $request){
    $categoryResponse = News::getallnews($request);
		print $categoryResponse;
	}

    public function login(Request $request) {
        $email = $request->email;
    		$is_staff = $request->is_staff;
    		$password = $request->password;
    
    		$userInfo = array("email" => $email, "password" => $password);
    		
    		if (Auth::attempt($userInfo)) {
    			$userExists = DB::table('users')->where('email', $email)->where('status', '1')->first();	
    			
    			if (count((array) $userExists)>0) {
    				return json_encode(['status' => 'success', 
    														'message' => 'logged in', 
    														'data' => [
    																	'id' => $userExists->id, 
    																	'fullname' => "{$userExists->first_name} {$userExists->last_name}", 
    																	'email' => $userExists->email, 
    																	'admin_type' => 1, 
    																	'status' => $userExists->status,
    																	'sites' => DB::table('sites')->join('user_sites','user_sites.site_id','sites.id')->where('user_sites.user_id',$userExists->id)->get()
    																]
    													]);
    				}
    		}
    
        return json_encode(['status' => 'failed', 'message' => 'email or password incorrect', 'data' => []]);
	}
	
	/*
	public function audits(Request $request){
		$data['audits'] = Audit::get();
		return json_encode(['status' => 'success', 'data' => $data]);
	}

	public function audit_questions(Request $request){
		$data['audit'] = Audit::find($request->audit_id);
		// $data['form_data'] = \DB::table('audit_questions2')->where('audit_id',$request->id)->get();
		$data['form_data'] = \DB::table('audit_questions')->where('audit_id',$request->audit_id)->get();
		// $data['answers'] = \DB::table('audit_answers')->where('audit_id',$request->id)->get();

		return json_encode(['status' => 'success', 'heading' => $data['audit']->heading, 'questions' => $data['form_data']]);
	}

	public function store(Request $request) {
	    
	    return $request->all();
	    
    	$audit_id = $request->get('audit_id');
		$data = $request->get('data');
		$signatures = $request->get('signatures');
		$user_id = $request->get('user_id');
		$site_id = $request->get('site_id');
		
		foreach( $data as $key => $field) {
			AuditAnswer::updateOrCreate([['audit_id', $audit_id],['question_id', $key],['user_id', $user_id]],[
				'fied' => null,
				'audit_id' => $audit_id,
				'question_id' => $key,
				'value' => $field,
				'user_id' => $user_id,
				'site_id' => $site_id
			]);
		}

		foreach ($signatures as $signature) {

			$data = AuditAnswer::where([['audit_id', $audit_id],['question_id', $signature['id']],['user_id', 'user_id']])->update(['value' => $signature['data']]);

		}

		return response()->json(['status' => 1, 'data' => $audit_id ]);
	}
	
	
	public function sites() {
		$sites = DB::table('sites')->orderBy('site_name','ASC')->get();

		return response()->json(['status' => 1, 'data' => $sites]);
	}
	*/
	
	public function all_audits(Request $request){
		$data['audits'] = Audit::where('status',1)->orderBy('heading','ASC')->get();
											
		return json_encode(['status' => 'success', 'data' => $data]);
	}
	
	public function all_audits2(Request $request){

		$regions = Audit::where("status",1)->groupBy('region')->orderBy('region','ASC')->get();
		
		$audits = Audit::where("status",1)->orderBy('heading','ASC')->get();

		$data = [];
		$count = 0;
		foreach ($regions as $region) {
			foreach ($audits as $audit) {
				if ($region->region == $audit->region) {
					$data[$audit->region][] = $audit;
				}
			}
			$count++;
		}
		
		return json_encode(['status' => 'success', 'data' => $data]);
	}
	
	public function user_audits(Request $request)
	{
		$audit = Audit::find($request->audit_id);

// 		$audits = DB::table('audit_sites')
// 		->select('audit_sites.*', 'audits.heading', 'audits.description','sites.site_name')
// 		->join('sites','sites.id','=','audit_sites.site_id')
// 		->join('audits','audits.id','=','audit_sites.audit_id')
// 		->where('user_id', '=', $request->user_id)
// 		->where('audit_id', '=', $request->audit_id)
// 		->orderBy('audit_sites.updated_at','DESC')
// 		->groupBy('audit_sites.site_id')
// 		->get();
		
		$sites = DB::table('sites')
// 			->join('user_sites','user_sites.site_id','sites.id')
// 			->whereNotIn('sites.id', $audits->pluck('site_id'))
// 			->where('user_sites.user_id', $request->user_id)
			->orderBy('site_name','ASC')
			->get();
		
// 		$new = $audits->toArray();

// 		$data['audits'] = $new;
		
		foreach($sites as $site)
		{
			$data['audits'][] = [
				'id' => 0, 
				'audit_id' => $request->audit_id,
				'site_id' => $site->id, // $site->id
				'user_id' => $request->user_id,
				'status'	=> 0,
				'created_at' => null,
				'updated_at' => null,
				'heading'	=> $audit->heading,
				'description' => $audit->description,
				'site_name' => $site->site_name
			];
		}

		return json_encode(['status' => 'success', 'data' => $data]);
	}

	public function user_site_audits(Request $request)
	{
		$audits = DB::table('audit_sites')
		->select('audit_sites.*', 'audits.heading', 'audits.description','sites.site_name')
		->join('sites','sites.id','=','audit_sites.site_id')
		->join('audits','audits.id','=','audit_sites.audit_id')
		->where('audit_sites.audit_id', '=', $request->audit_id)
		->where('audit_sites.site_id', '=', $request->site_id)
		->where('audit_sites.user_id', '=', $request->user_id)
		->orderBy('audit_sites.id','DESC')
		->get();

		$new = $audits->toArray();
		$data['audits'] = $new;

		return json_encode(['status' => 'success', 'data' => $data]);
	}
	
    /*
	public function user_audits(Request $request)
	{
		$audit = Audit::find($request->audit_id);

		$audits = DB::table('audit_sites')
		->select('audit_sites.*', 'audits.heading', 'audits.description','sites.site_name')
		->join('sites','sites.id','=','audit_sites.site_id')
		->join('audits','audits.id','=','audit_sites.audit_id')
		->where('user_id', '=', $request->user_id)
		->get();

		$sites = DB::table('sites')
			->join('user_sites','user_sites.site_id','sites.id')
			->whereNotIn('sites.id',$audits->pluck('site_id'))
			->where('user_sites.user_id',$request->user_id)
			->orderBy('site_name','ASC')
			->get();
		
		$new = $audits->toArray();

		$data['audits'] = $new;
		
		foreach($sites as $site)
		{
			$data['audits'][] = [
				'id' => 0,
				'audit_id' => $request->audit_id,
				'site_id' => $site->id,
				'user_id' => $request->user_id,
				'status'	=> 0,
				'created_at' => null,
				'updated_at' => null,
				'heading'	=> $audit->heading,
				'description' => $audit->description,
				'site_name' => $site->site_name
			];
		}

		return json_encode(['status' => 'success', 'data' => $data]);
	}
	*//*
	public function audit_questions(Request $request){
		$data['audit'] = Audit::find($request->audit_id);
		$audit_site = DB::table('audit_sites')->where('audit_id',$request->audit_id)->first();
		// $data['form_data'] = \DB::table('audit_questions2')->where('audit_id',$request->id)->get();
		// $data['answers'] = \DB::table('audit_answers')->where('audit_id',$request->id)->get();
		// $data['form_data'] = \DB::table('audit_questions')->where('audit_id',$request->audit_id)->get();
// 		return $audit_site;
		if(count((array) $audit_site) > 0){
		  //  echo 'ANSWERS';
		    $data['form_data'] = DB::table('audit_questions')
								->leftJoin('audit_answers','audit_answers.question_id','=','audit_questions.id')
								->select('audit_questions.*','audit_answers.field','audit_answers.value')
								->where('audit_answers.audit_id',$request->audit_id)
								->where('audit_answers.user_id',$audit_site->user_id)
								->get();
								
			return json_encode([
						'status' => 'success', 
						'heading' => $data['audit']->heading, 
						'site_id' => $audit_site->site_id, 
						'site_name' => DB::table('sites')->where('id',$audit_site->site_id)->first(),
						'user_id' => $audit_site->user_id, 
						'questions' => $data['form_data']
					 ]);
		}else{
    	    $data['form_data'] = DB::table('audit_questions')
				// 		->leftJoin('audit_answers','audit_answers.question_id','=','audit_questions.id')
				// 		->select('audit_questions.*','audit_answers.field','audit_answers.value')
				// 		->where('audit_answers.audit_id',$request->audit_id)
				// 		->where('audit_answers.user_id',$audit_site->user_id)
				        ->where('audit_id',$request->audit_id)
						->get();
						
				return json_encode([
						'status' => 'success', 
						'heading' => $data['audit']->heading, 
						'site_id' => 0, 
						'site_name' => '',
						'user_id' => 0, 
						'questions' => $data['form_data']
					 ]);
					 
		}
	}
    */
    
    public function audit_questions(Request $request)
	{
		$data['audit'] = Audit::find($request->audit_id);
		
		$audit_site = DB::table('audit_sites')->where('audit_id',$request->audit_id)->where('site_id',$request->site_id)->first();
		
		if(count((array) $audit_site) > 0){
		// $data['form_data'] = \DB::table('audit_questions2')->where('audit_id',$request->id)->get();
		// $data['answers'] = \DB::table('audit_answers')->where('audit_id',$request->id)->get();
		// $data['form_data'] = \DB::table('audit_questions')->where('audit_id',$request->audit_id)->get();
		$data['form_data'] = DB::table('audit_questions')
													->leftJoin('audit_answers','audit_answers.question_id','=','audit_questions.id')
													->select('audit_questions.*','audit_answers.field','audit_answers.value')
													->where('audit_answers.audit_id',$request->audit_id)
													->where('audit_answers.site_id',$request->site_id)
													->where('audit_answers.user_id',$audit_site->user_id)
													->get();
		}else{
			$data['form_data'] = DB::table('audit_questions')
			                                ->where('audit_id', $request->audit_id)
											->get();
		}
		$site_id = isset($audit_site->site_id)?$audit_site->site_id:$request->site_id;

		return json_encode([
							'status' => 1, 
							'heading' => $data['audit']->heading, 
							'site_id' => $site_id, 
							'site_name' => DB::table('sites')->where('id',$site_id)->first(),
							'user_id' => isset($audit_site->user_id)?$audit_site->user_id:Auth::id(), 
							'questions' => $data['form_data']
						 ]);
	}

	public function audit_questions2(Request $request)
	{
		$audit_site = AuditSite::find($request->audit_site_id);

		$data['audit'] = Audit::find($audit_site->audit_id);
		
		
		$data['form_data'] = DB::table('audit_questions')
												->leftJoin('audit_answers','audit_answers.question_id','=','audit_questions.id')
												->select('audit_questions.*','audit_answers.field','audit_answers.value')
												->where('audit_answers.site_audit_id',$audit_site->id)
												->get();
		
		if(count($data['form_data']) == 0){
			$data['form_data'] = DB::table('audit_questions')
												->where('audit_id', $audit_site->audit_id)
												->get();
		}

		return json_encode([
							'status' => 1, 
							'heading' => $data['audit']->heading, 
							'site_id' => $audit_site->site_id, 
							'site_name' => DB::table('sites')->where('id',$audit_site->site_id)->first(),
							'user_id' => $audit_site->user_id, 
							'questions' => $data['form_data']
						 ]);
	}
	
	
	public function store(Request $request) {
	    
	    return response()->json(['status' => 0, 'data' => $audit_id, 'message' => 'Unsuccessful, Please update app.' ]);
	    	
    	$path = './images/media/audit_images/';
        	
		$audit_id = $request->get('audit_id');
		$signatures = $request->get('signatures');
		$user_id = $request->get('user_id');
		$site_id = $request->get('site_id');
		$question_data = $request->get('question_data');
		$images = $request->get('images');
		$audit_status = $request->get('audit_status')?$request->get('audit_status'):0;
        $notes = $request->get('notes');
		$actions = $request->get('actions');
		$location_latitude = $request->get('location_latitude');
		$location_longitude = $request->get('location_longitude');


		$audit_info = DB::table('audit_sites')->where('site_id', $site_id)->where('audit_id', $audit_id)->first();

		if (count((array) $audit_info) > 0) {
			$id = $audit_info->id;
			DB::table('audit_sites')->where('id', $id)->update(['status' => $audit_status]);
		}else{
			$id = DB::table('audit_sites')->insertGetId(['audit_id' => $audit_id, 'site_id' => $site_id, 'user_id' => $user_id, 'status' => $audit_status, 'latitude' => $location_latitude, 'longitude' => $location_longitude]);
		}
        
        
		foreach( $question_data as $key => $field) {
			AuditAnswer::updateOrCreate([['audit_id', $audit_id],['question_id', $key],['user_id', $user_id],['site_audit_id', $id]],[
				'field' => null,
				'site_audit_id' => $id,
				'audit_id' => $audit_id,
				'question_id' => $key,
				'value' => $field,
				'user_id' => $user_id,
				'site_id' => $site_id
			]);
		}
        
    	if ($notes) {
			foreach ($notes as $note) {

				DB::table('audit_notes')->insert([
					'site_audit_id' => $id,
					'question_id' => $note['question_id'],
					'note' => $note['note'],
					'user_id' => $user_id,
					'site_id' => $site_id
				]);

			}
		}
		
       	if ($images) {
			foreach ($images as $question_id => $base64) {

					$filename = "q{$question_id}_{$id}_".time().'_'.uniqid().'.png';	
					
					$image_parts = explode("data:image/jpeg;base64,", $base64['image']);

					$image_base64 = base64_decode($image_parts[1]);
					$completed = file_put_contents($path.$filename, $image_base64);

					DB::table('audit_images')->insert([
						'site_audit_id' => $id,
						'question_id' => $base64['question'],
						'image_type' => 'png',
						'image_path' => $path.$filename,
					]);
			}
		}
		

		foreach ($signatures as $signature) 
		{
			$data = AuditAnswer::where([['audit_id', $audit_id],['question_id', $signature['id']],['user_id', $user_id]])->update(['value' => $signature['data'], 'value2'=>$signature['signatureAuthor']]);
		}

    	if ($audit_status == 1) {	
	   // 	event(new AuditCompleted($id));
		}
		
		return response()->json(['status' => 1, 'data' => $audit_id, 'message' => 'Your Audit has been submitted.' ]);
	}
    
    public function store2(Request $request) {
    	$path = './images/media/audit_images/';
        	
        $audit_site_id = $request->get('audit_site_id');
		$audit_id = $request->get('audit_id');
        
		$signatures = $request->get('signatures');
		$user_id = $request->get('user_id');
		$site_id = $request->get('site_id');
		$question_data = $request->get('question_data');
		$images = $request->get('images');
		$audit_status = $request->get('audit_status')?$request->get('audit_status'):0;
        $notes = $request->get('notes');
		$actions = $request->get('action');
		$location_latitude = $request->get('location_latitude');
		$location_longitude = $request->get('location_longitude');

		AuditSite::where('id', $audit_site_id)->update([
			'audit_id' => $audit_id, 
			'site_id' => $site_id, 
			'user_id' => $user_id, 
			'status' => $audit_status,
			'end_latitude' => $location_latitude, 
			'end_longitude' => $location_longitude
		]);
		
		$audit_method = ($audit_status == 1)? 'completed' : 'saved';

		AuditLog::insert([
			'audit_site_id' => $audit_site_id, 
			'user_id' => $user_id, 
			'status' => $audit_status,
			'method' => $audit_method,
			'latitude' => $location_latitude, 
			'longitude' => $location_longitude
		]);
        
		foreach( $question_data as $key => $field) {
		    if($field != null){
    			AuditAnswer::updateOrCreate([['site_audit_id', $audit_site_id],['question_id', $key],['user_id', $user_id]],[
    				'field' => null,
    				'site_audit_id' => $audit_site_id,
    				'audit_id' => $audit_id,
    				'question_id' => $key,
    				'value' => $field,
    				'user_id' => $user_id,
    				'site_id' => $site_id
    			]);
		    }else{
		        AuditAnswer::updateOrCreate([['site_audit_id', $audit_site_id],['question_id', $key],['user_id', $user_id]],[
    				'field' => null,
    				'site_audit_id' => $audit_site_id,
    				'audit_id' => $audit_id,
    				'question_id' => $key,
    				'user_id' => $user_id,
    				'site_id' => $site_id
    			]);
		    }
		}
        
    	if ($notes) {
			foreach ($notes as $note) {

				// DB::table('audit_notes')->insert([
				// 	'site_audit_id' => $audit_site_id,
				// 	'question_id' => $note['question_id'],
				// 	'note' => $note['note'],
				// 	'user_id' => $user_id,
				// 	'site_id' => $site_id
				// ]);
                
            	AuditNote::updateOrCreate([['site_audit_id', $audit_site_id],['question_id', $note['question_id']]],[
					'site_audit_id' => $audit_site_id,
					'question_id' => $note['question_id'],
					'note' => $note['note'],
					'user_id' => $user_id,
					'site_id' => $site_id
				]);
				
			}
		}
		
		if ($actions) {
			foreach ($actions as $action) {

				// DB::table('audit_actions')->insert([
				// 	'site_audit_id' => $audit_site_id,
				// 	'question_id' => $action['question_id'],
				// 	'action' => $action['action'],
				// 	'action_date' => $action['action_date'],
				// 	'user_id' => $user_id,
				// 	'site_id' => $site_id
				// ]);
                
                AuditAction::updateOrCreate([['site_audit_id', $audit_site_id],['question_id', $action['question_id']]],[
					'site_audit_id' => $audit_site_id,
					'question_id' => $action['question_id'],
					'action' => $action['action'],
					'action_date' => $action['action_date'],
					'user_id' => $user_id,
					'site_id' => $site_id
				]);
				
			}
		}
	
		foreach ($signatures as $signature) 
		{
		    if(isset($signature['data'])){
		        $signature_data = $signature['data'];
		    }else{
		        $signature_data = '';
		    }
		     if(isset($signature['signatureAuthor'])){
		        $signature_name = $signature['signatureAuthor'];
		    }else{
		        $signature_name = '';
		    }
		  //  if($signature_data != '') {
			 //   $data = AuditAnswer::where([['site_audit_id', $audit_site_id],['question_id', $signature['id']]])->update(['value' => $signature_data, 'value2'=>$signature['signatureAuthor']]);
		  //  }
		    if($signature_data != '') {
				$data = AuditAnswer::where([['site_audit_id', $audit_site_id],['question_id', $signature['id']]])->update(['value' => $signature_data]);
			}
			if($signature_name != '') {
				$data = AuditAnswer::where([['site_audit_id', $audit_site_id],['question_id', $signature['id']]])->update(['value2'=>$signature_name]);
			}
		}

    	if ($audit_status == 1) {
    	//	event(new AuditCompleted($audit_site_id));
		}
		
		return response()->json(['status' => 1, 'data' => $audit_id, 'message' => 'Your Audit has been submitted.' ]);
	}
	
    public function upload_images(Request $request) {
    	$path = 'images/media/audit_images';
    
        $audit_site_id = $request->get('audit_site_id')?$request->get('audit_site_id'):0;
    	if($request->hasFile('file')){
            $file = $request->file('file');
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);	
						
			$file_question_id = explode("_", $filename);

// 			DB::table('audit_images')->insert([
// 				'site_audit_id' => $file_question_id[1],
// 				'question_id' => $file_question_id[0],
// 				'image_type' => 'png',
// 				'image_path' => $path.'/'.$filename,
// 			]);
			
			AuditImage::updateOrCreate([['site_audit_id', $file_question_id[1]],['question_id', $file_question_id[0]],['image_path', $path.'/'.$filename]],[
				'site_audit_id' => $file_question_id[1],
				'question_id' => $file_question_id[0],
				'image_type' => 'png',
				'image_path' => $path.'/'.$filename,
			]);
			
    	    $data = json_encode(['status' => 1, 'message' => 'Upload and move success']);
    	}else{
	       $data = json_encode(['status' => 0, 'message' => 'no file attached']);
    	}
   
    	return $data;
	  
    }
    
	public function user_sites(Request $request)
	{
		$sites = DB::table('sites')->join('user_sites','user_sites.site_id','sites.id')->where('user_sites.user_id',$request->user_id)->orderBy('site_name','ASC')->get();

		if($sites->isEmpty()){
			return response()->json(['status' => 0, 'data' => [], 'message' => 'No sites found.']);
		}

		return response()->json(['status' => 1, 'data' => $sites]);
	}

	public function all_sites() {
		$sites = DB::table('sites')->orderBy('site_name','ASC')->get();

		return response()->json(['status' => 1, 'data' => $sites]);
	}

	
	public function duplicate_audits()
	{
	    return false;
	    $data = DB::table('audit_questions')->where('audit_id',5)->get();
	    foreach($data as $key => $audit){
	        DB::table('audit_questions')->insert([
				'audit_id' => 7,
				'question_name' => $audit->question_name,
				'question' => $audit->question,
				'question_type' => $audit->question_type,
				'question_type_two' => $audit->question_type_two,
				'question_required' => $audit->question_required,
				'question_points' => $audit->question_points,
				'question_compliance' => $audit->question_compliance
			]);
	    }
	}
	
	public function show_image(Request $request) {
    
        $image = $request->image;
        echo "<img src='".asset('images/media/audit_images/'.$image)."' width='150px' />";
        
	}
	
    public function start_audit(Request $request)
	{
		$audit_id = $request->get('audit_id');
		$user_id = $request->get('user_id');
		$site_id = $request->get('site_id');

		$location_latitude = $request->get('location_latitude');
		$location_longitude = $request->get('location_longitude');
		
		$id = DB::table('audit_sites')->insertGetId([
			'audit_id' => $audit_id, 
			'site_id' => $site_id, 
			'user_id' => $user_id, 
			'status' => 0,
			'latitude' => $location_latitude, 
			'longitude' => $location_longitude,
			]);
		
		AuditLog::insert([
			'audit_site_id' => $id, 
			'user_id' => $user_id, 
			'status' => 0,
			'method' => 'start',
			'latitude' => $location_latitude, 
			'longitude' => $location_longitude
		]);

		$audit_info = DB::table('audit_sites')->where('id', $id)->first();

		return response()->json(['status' => 1, 'data' => $audit_info]);
	}
    
	public function pending_audits(Request $request)
	{
		$audits = AuditSite::with(['audit','site'])->notCompleted()->where('user_id', $request->user_id)->orderBy('updated_at','DESC')->get();

		if($audits->isEmpty()){
			return ['status' => 0, 'data' => [], 'message' => 'No current audits.'];
		}
		
		$data = [];
		foreach($audits as $audit){
			$data[($audit->status == 0) ? 'pending':'saved'][] = $audit;
		}
		krsort($data);
		
		return ['status' => 1,'data' => $data, 'message' => 'Current audits found.'];

	}
   
	public function completed_audits(Request $request)
	{
		$audits = AuditSite::with(['audit','site','stats'])->completed()->where('user_id', $request->user_id)->orderBy('updated_at','DESC')->get();

		if($audits->isEmpty()){
			return ['status' => 0, 'data' => [], 'message' => 'No completed audits.'];
		}
        
    	$data = [];
		foreach($audits as $audit){
			$data[$audit->site->site_name][] = $audit;
		}
		ksort($data);
		
		return ['status' => 1,'data' => $data, 'message' => 'Completed audits found.'];
	}
	
	
	public function destroy(Request $request) {
	
		$audit = AuditSite::find($request->audit_site_id);
		
		if ($audit && $audit->status == 0) {
			$data = AuditSite::find($request->audit_site_id)->delete();
			
			if ($data > 0) {
				return ['status' => 1, 'result' => [], 'message' => 'Audit has been deleted'];
			}
		}

		return ['status' => 0, 'result' => [], 'message' => 'Unable to delete audit'];

	}
	
    public function generate_retail_users() {
        return false;
		$result = array();
		$message = array();
		$errorMessage = array();
		
		$sites = DB::table('sites')->where('location_id','>',0)->get();
		
		$temp = '';
		
		$data1 = "Site Name; Bc; BC NAME; Retailer; Manager; BC Email; Password; Url; App Download <br/>";
		$data = "Site Name; Bc; BC NAME; Retailer; Manager; BC Email; Password; Url; App Download <br/>";
		$url = url('admin/login');
		$app_url = "https://jnzsoftware.co.za/apk/audit_app/app.apk";
		
		$count = 0;
		
		foreach($sites as $site){
			$bcs[$site->bc_email][] = $site;
		}
		
		foreach($bcs as $bc => $bc_sites){
			
			if(!empty($bc)){
				
				$existEmail = DB::table('users')->where('email', '=', $bc)->get();
				if(count($existEmail)>0){
						
				}else{
						
					$names = explode(" ",$bc_sites[0]->bc_name);
					$first_name = $names[0];

						$uploadImage = '';		
						$passwordraw =  $first_name . substr(md5(uniqid(mt_rand(), true)) , 0, 4);
						$bc_passwordraw = strtolower($passwordraw);
						$bc_password = Hash::make($bc_passwordraw);
						
						$last_name = '';
						if(isset($names[1])){
								$last_name = $names[1];
						}
						// echo $first_name . " | " . $bc_passwordraw . "<br/>";
						
						$bc_id = DB::table('users')->insertGetId([
												'role_id'			=>		12,
												'user_name'		 		    =>  $first_name,
												'first_name'		 		=>   $first_name,
												'last_name'			 		=>   $last_name,
												'phone'	 					=>	 $bc_sites[0]->landline,
												'email'	 					=>   $bc_sites[0]->bc_email,
												'password'		 			=>   $bc_password,
												'status'		 	 		=>   1,
												'avatar'	 					=>	 $uploadImage
										]);
							if($bc_id > 0){
								foreach($bc_sites as $bc_site){
									DB::table('user_sites')->insert(['user_id' => $bc_id, 'site_id' => $bc_site->id]);
									$data1 .= "$bc_site->site_name; $bc_site->bc; $bc_site->bc_name; $bc_site->retailer; $bc_site->manager; $bc_site->bc_email; $bc_passwordraw; $url; $app_url <br/>";
								}
							}
							$data .= $bc_sites[0]->site_name.";". $bc_sites[0]->bc.";". $bc_sites[0]->bc_name.";". $bc_sites[0]->retailer.";". $bc_sites[0]->manager.";". $bc_sites[0]->bc_email."; $bc_passwordraw;". $url ."; $app_url <br/>";
							$count ++;
					}
			}
			
			// $data .= $bc_sites[0]->site_name.";". $bc_sites[0]->bc.";". $bc_sites[0]->bc_name.";". $bc_sites[0]->retailer.";". $bc_sites[0]->manager.";". $bc_sites[0]->bc_email."; pass;". $url ."; $app_url <br/>";
		}
		
		echo $data1 . "<br/><br/><br/>";
		echo $data;
	}		
    
    public function update_report(Request $request) {

		$audit = AuditSite::find($request->audit_site_id);
		
		if ($audit->status == 1) {
			event(new AuditCompleted($request->audit_site_id));
			return ['status' => 1,'result' => 'audit updated'];
		}
		return ['status' => 0,'result' => 'audit not updated'];
	}
	
    public function email_audit(Request $request) {
        
        $audit = AuditSite::with(['audit','user','site'])->find($request->get('id'));
        
        $retailer_email_addresses = explode(";", $audit->site->email);
        
        $audit_filename = $audit->document;
      
        if($audit_filename == ''){
            if($audit->status == 1){
                event(new AuditCompleted($request->get('id')));
    			return ['status' => 1,'message' => 'Audit has been updated and emailed']; 
            }else{
                return ['status' => 0, 'message' => 'unable to find audit'];
            }
        }
        
        $retailer_emails = explode(";",$audit->site->email);
        
        $bc_email_address = $audit->user->email;
        $bc_name = $audit->user->first_name;
        $opts_email = $audit->user->opts_manager;
        
        $subject = "Audit Report: ".$audit->audit->heading." - " . $audit->site->site_name;
         
        Mail::send("mail.audit_report", ['data' => $audit], function ($mailer) use ($subject, $bc_email_address, $bc_name, $retailer_emails, $opts_email, $audit, $audit_filename) {
            $mailer->from('noreply@auditapp.jnzsoftware.co.za', 'Freshstop Auditor');
            $mailer->to($bc_email_address);
            
            if($opts_email != null) {
                $mailer->cc($opts_email);
            }
            
            if(!empty($retailer_emails)){
                foreach($retailer_emails as $retailer_email){
                    if($retailer_email != ' '){
                        $mailer->cc(trim($retailer_email));
                        break;
                    }
                }
            }
            
            // $mailer->bcc(['jamie@jnz.co.za','audits@freshstopapp.co.za']);
            $mailer->bcc('audits@freshstopapp.co.za');
            $mailer->subject($subject);
            $mailer->attach($audit_filename);
        });
        
        if( count(Mail::failures()) > 0 ) {

            foreach(Mail::failures as $email_address) {
            //   echo "$email_address <br />";
                return ['status' => 1, 'message' => 'Email address failed: '.$email_address];
            }
        
        } else {
            return ['status' => 1, 'message' => 'Audit report has been sent'];
        }

    }	
    
    
    
     public function email_resend_audits() {
        exit;
        $audits = AuditSite::with(['audit','user','site'])->where('status',1)->where('created_at','>','2021-04-16')->where('created_at','<','2021-04-17')->get();
        // return $audits;
        foreach($audits as $audit){
        
        $retailer_email_addresses = explode(";", $audit->site->email);
        
        $audit_filename = $audit->document;
      
        if($audit_filename == ''){
            // return ['status' => 0, 'message' => 'unable to find audit'];
            continue;
        }
        
        $bc_email_address = $audit->user->email;
        $bc_name = $audit->user->first_name;
        
        Mail::send("mail.audit_report", [], function ($mailer) use ($bc_email_address, $bc_name, $audit, $audit_filename) {
            $mailer->from('noreply@auditapp.jnzsoftware.co.za', 'Freshstop AuditAPP');
            $mailer->to($bc_email_address);
            // $mailer->bcc('jamie@jnz.co.za');
            $mailer->subject('Audit Report: ' . $audit->audit->heading);
            $mailer->attach($audit_filename);
        });
        
        if( count(Mail::failures()) > 0 ) {

            foreach(Mail::failures as $email_address) {
                // return ['status' => 1, 'message' => 'Email address failed: '.$email_address];
                continue;
            }
        
        } else {
            // return ['status' => 1, 'message' => 'Audit report has been sent'];
        }
        
        }

    }	
    
    
     public function sendAuditReport(Request $request) {
        return false;
		$audit = AuditSite::find($request->audit_site_id);
		
		if ($audit->status == 1) {
			event(new AuditCompleted($request->audit_site_id));
			return ['status' => 1,'result' => 'audit sent'];
		}
		
		return ['status' => 0,'result' => 'audit not updated'];
	}
    
    
    
    
    public function review_audit_data(){
        $site_audit_id = 198;
        
        $audit_info = AuditSite::with('user')->find($site_audit_id);
        // return $audit_info;
        $data['audit'] = AuditSite::with(['audit','site'])->find($site_audit_id);
        // $data['audit'] = $data['audit']->first();
        
        return $data['audit'];
    }
    
    
    
    
    public function resizeImage($image){
        
        return [];
        
		$image = $image?substr($image,26):'';
		
		$origin = public_path('images/media/audit_images/'.$image);

		$newimg = public_path('images/media/audit_images_resized/_'.$image);
		$w = 0;
		$h = 0;
		$m = 600;
		
		$info = getimagesize($origin);
		
		list($width, $height) = $info;
		
		if($width > $m){
			if ($m == 0){
				$w = ($w>0?$w:($width > 1800? ($width/3.5):($width < 700?$width:($width/2))));
				$h = ($h>0?$h:($height * ($w/$width)));
			}else{
				$w = $m;
				$h = ($h>0?$h:($height * ($w/$width)));
			}
		}else{
			// $w = $width;
			// $h = $height;
			// $this->fileImage = $image;
			// $this->compress_single();
			return json_encode(false);
		}
		
		if ($info['mime'] == 'image/jpeg'){ 
		    // if($this->imageIsValid($this->origin_directory.$image)){
			    $src = imagecreatefromjpeg($origin);
		    // }
		    
		}elseif ($info['mime'] == 'image/png') {
		    // if($this->imageIsValid($this->origin_directory.$image)){
			    $src = imagecreatefrompng($origin);
		    // }
		}
		if(isset($src)){  
			echo 'resizing: '. $image .'<br/>';  
    		$dst = imagecreatetruecolor($w, $h);
    		imagecopyresampled($dst, $src, 0,0,0,0, $w, $h, $width, $height);
    		
    		if ($info['mime'] == 'image/jpeg')
    			$resized = imagejpeg($dst, $newimg);
    		elseif ($info['mime'] == 'image/png')
    			$resized = imagepng($dst, $newimg);
		}
	}
	
	public function resizeImages(){
        
        
        $images = scandir(public_path('images/media/audit_images/'));
        // return $images;
        foreach($images as $image)
        {
            if(!is_dir($image)){
                
            		
            		$origin = public_path('images/media/audit_images/'.$image);
            
            		$newimg = public_path('images/media/audit_images_resized/_'.$image);
            		$w = 0;
            		$h = 0;
            		$m = 600;
            		
            		$info = getimagesize($origin);
            		
            		list($width, $height) = $info;
            		
            		if($width > $m){
            			if ($m == 0){
            				$w = ($w>0?$w:($width > 1800? ($width/3.5):($width < 700?$width:($width/2))));
            				$h = ($h>0?$h:($height * ($w/$width)));
            			}else{
            				$w = $m;
            				$h = ($h>0?$h:($height * ($w/$width)));
            			}
            		}else{
            			// $w = $width;
            			// $h = $height;
            			// $this->fileImage = $image;
            			// $this->compress_single();
                     // 			return json_encode(false);
                     echo 'failed: ' . $image ."<br/>";
            		}
            		
            		if ($info['mime'] == 'image/jpeg'){ 
            		    // if($this->imageIsValid($this->origin_directory.$image)){
            			    $src = imagecreatefromjpeg($origin);
            		    // }
            		    
            		}elseif ($info['mime'] == 'image/png') {
            		    // if($this->imageIsValid($this->origin_directory.$image)){
            			    $src = imagecreatefrompng($origin);
            		    // }
            		}
            		if(isset($src)){  
            			echo 'resizing: '. $image .'<br/>';  
                		$dst = imagecreatetruecolor($w, $h);
                		imagecopyresampled($dst, $src, 0,0,0,0, $w, $h, $width, $height);
                		
                		if ($info['mime'] == 'image/jpeg')
                			$resized = imagejpeg($dst, $newimg);
                		elseif ($info['mime'] == 'image/png')
                			$resized = imagepng($dst, $newimg);
            		}
                echo 'Passed: ' . $image ."<br/>";
            }   
        }
        
        
	}
    
}
