<?php

namespace App\Listeners;

use App\Events\AuditCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AuditSite;
use Mail;
use DB;

class generateAuditStatistics
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuditCompleted  $event
     * @return void
     */
    public function handle(AuditCompleted $event)
    {
        $site_audit_id = $event->id;
        $audit_info = AuditSite::with(['audit','site','user'])->find($site_audit_id);

        $data['audit'] = $audit_info; //AuditSite::with(['audit','site'])->find($site_audit_id);

        $data['questions'] = \DB::table('audit_questions')->where('audit_id',$audit_info->audit_id)->get()->toArray();
        $data['answers'] = \DB::table('audit_answers')->where('site_audit_id',$audit_info->id)->get();
        $data['images'] = \DB::table('audit_images')->where('site_audit_id',$data['audit']->id)->get();
        $data['notes'] = \DB::table('audit_notes')->where('site_audit_id', $data['audit']->id)->get();

        $data['data'] = [];
        $data['point_data'] = [];
        $data['form_data'] = [];
        $total_points = 0;
        $valid_points = 0;
        $invalid_points = 0;
        $total_headers = 0;
        $failed_items = 0;
        $data['total_valid'] = 0;
        $data['total_invalid'] = 0;
        $data['total_points'] = 0;
        $data['total_inspection_score'] = 0;
        foreach ($data['questions'] as $question) {
              $question->answer = null;
              $question->answer2 = null;

            foreach ($data['answers'] as $answers) {
                if ($question->id == $answers->question_id) {
                    $question->answer = $answers->value;
                    $question->answer2 = $answers->value2;
                    break;
                }
            }
            foreach ($data['images'] as $image) {
                if ($question->id == $image->question_id) {
                    $question->images[] = $image->image_path;
                    $this->resizeImage($image->image_path);
                }
            }
            foreach ($data['notes'] as $note) {
                if ($question->id == $note->question_id) {
                    $question->notes[] = $note;
                }
            }

            $data['data'][] = (array) $question;
        }

        $temp_pointer = 0;
        foreach($data['data'] as $key => $points) {

            if ($points['question_type_two'] == 'header') {
                $pointer = true;
                $temp_pointer = $key;
            }

            if ($points['question_type_two'] != 'header') {
                $total_points += $points['question_points'];
                ($points['answer'] == 'YES') ? $valid_points += $points['question_points']: $invalid_points += $points['question_points'];
                $failed_items += ($points['answer'] == 'NO')?1:0;
                $pointer = true;
            }

            $points['total_points'] = $total_points;
            $points['valid_points'] = 0;
            $points['invalid_points'] = 0;

            $data['point_data'][] = $points;

            if(!in_array($audit_info->audit->audit_type,['contact','snapshot'])){
                if ($pointer) {

                    $data['point_data'][$temp_pointer]['total_points'] += $total_points;
                    $data['point_data'][$temp_pointer]['valid_points'] += $valid_points;
                    $data['point_data'][$temp_pointer]['invalid_points'] += $invalid_points;

                    $pointer = false;
                    $total_points = 0;
                    $valid_points = 0;
                    $invalid_points = 0;
                }
            }
        }
        unset($data['data']);
        unset($data['answers']);
        unset($data['questions']);

        $data['failed_items'] = $failed_items;

        $data['form_data'] = (object) $data['point_data'];

        $object = new \stdClass();
        foreach ($data['point_data'] as $key => $value) {
          $data['form_data']->{$key} = (object) $value;
          if(!in_array($data['form_data']->{$key}->question_type_two,['signature','file']) && $data['form_data']->{$key}->question_type_two == 'header'){
              if($data['form_data']->{$key}->total_points > 0){
                $data['total_inspection_score'] += ($data['form_data']->{$key}->valid_points / $data['form_data']->{$key}->total_points);
              }else{
                  $data['total_inspection_score'] += 0;
              }
            $data['total_valid'] += $data['form_data']->{$key}->valid_points;
            $data['total_invalid'] += $data['form_data']->{$key}->invalid_points;
            $data['total_points'] += $data['form_data']->{$key}->total_points;
            $total_headers += 1;
          }
        }

        unset($data['point_data']);

        // $data['total_inspection_score'] =  $data['total_inspection_score'] * $total_headers;
        if(!in_array($audit_info->audit->audit_type,['contact','snapshot'])){
            $data['total_inspection_score'] = ($data['total_valid'] / $data['total_points']) * 100;
        }

        DB::table('audit_stats')->insert([
            'audit_id' => $audit_info->audit_id,
            'audit_site_id' => $audit_info->id,
            'user_id' => $audit_info->user_id,
            'audit_date' => date('Y-m-d'),
            'total_valid' => $data['total_valid'],
            'total_failed' => $data['total_invalid'],
            'total_points' => $data['total_points'],
            'total_percentage' => $data['total_inspection_score'],
        ]);

        $pdf = \PDF::loadView('admin.snapshots.pdf', ['result' => $data]);
        $audit_filename = "audit_reports/audit_{$audit_info->id}_".date('YmdHis').".pdf";
        $pdf->save($audit_filename);

        AuditSite::where('id', $site_audit_id)->update(['document' => $audit_filename]);

        $retailer_emails = explode(";",$audit_info->site->email);

        $email = $audit_info->user->email;
        $name = $audit_info->user->first_name;
        $opts_email = $audit_info->user->opts_manager;

        $subject = "Audit Report: ".$audit_info->audit->heading." - " . $audit_info->site->site_name;

        Mail::send("mail.audit_report", ['data' => $audit_info], function ($mailer) use ($subject, $email, $name, $retailer_emails, $opts_email, $audit_filename) {
            $mailer->from('noreply@auditapp.jnzsoftware.co.za', 'Freshstop Auditor');
            $mailer->to($email);
            if ($opts_email != null) {
                $mailer->cc($opts_email);
            }
           if (!empty($retailer_emails)) {
                foreach($retailer_emails as $retailer_email){
                    if($retailer_email != ' '){
                        $mailer->cc(trim($retailer_email));
                        break;
                    }
                }
            }
            $mailer->bcc('audits@freshstopapp.co.za');
            $mailer->subject($subject);
            $mailer->attach($audit_filename);
        });


    }


        public function resizeImage($image){
		$image = $image?substr($image,26):'';

		$origin = public_path('images/media/audit_images/'.$image);

		$newimg = public_path('images/media/audit_images_resized/_'.$image);
		$w = 0;
		$h = 0;
		$m = 650;

		$info = getimagesize($origin);

		list($width, $height) = $info;

		if($width > $m){
			if ($m == 0){
				$w = ($w>0?$w:($width > 1800? ($width/3.5):($width < 700?$width:($width/2))));
				$h = ($h>0?$h:($height * ($w/$width)));
			}else{
				$w = $m;
				$h = ($h>0?$h:($height * ($w/$width)));
			}
		}else{
			// $w = $width;
			// $h = $height;
			// $this->fileImage = $image;
			// $this->compress_single();
			return json_encode(false);
		}

		if ($info['mime'] == 'image/jpeg'){
		    // if($this->imageIsValid($this->origin_directory.$image)){
			    $src = imagecreatefromjpeg($origin);
		    // }

		}elseif ($info['mime'] == 'image/png') {
		    // if($this->imageIsValid($this->origin_directory.$image)){
			    $src = imagecreatefrompng($origin);
		    // }
		}
		if(isset($src)){
    		$dst = imagecreatetruecolor($w, $h);
    		imagecopyresampled($dst, $src, 0,0,0,0, $w, $h, $width, $height);

    		if ($info['mime'] == 'image/jpeg')
    			$resized = imagejpeg($dst, $newimg);
    		elseif ($info['mime'] == 'image/png')
    			$resized = imagepng($dst, $newimg);
		}
	}

}
