<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
  use SoftDeletes;
  
  protected $fillable = [
      'site_name', 'bc','bc_name','bc_email', 'retailer','area','address','landline','cell_no','email','status'
  ];

}
