<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sites extends Authenticatable
{
    use Notifiable; use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'sites';
	
    protected $fillable = [
        'site_name', 'bc','bc_name','bc_email', 'retailer','area','address','landline','cell_no','email','status'
    ];
  
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
        // 'password', 'remember_token',
    // ];
	//use user id of admin
	protected $primaryKey = 'id';
	
    public function user(){
        return $this->belongsTo(User::class,'fk_user');
    }

}
