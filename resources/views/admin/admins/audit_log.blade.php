<!-- The Modal -->
<div class="modal-dialog">
	<div class="panel panel-default">
		    
    <!-- Modal Header -->
    <div class="panel-heading"  style="padding: 10px;">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Audit</h4>
		</div>

    <!-- Modal body -->
    <div class="modal-body" style="height: 500px; overflow: auto">
      <div class="row">
        <div class="col-xs-12">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Method</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Created</th>
              </tr>
            </thead>
            <tbody>
            @foreach($log as $l)
              <tr>
                <td>{{$l->method}}</td>
                <td>{{$l->latitude}}</td>
                <td>{{$l->longitude}}</td>
                <td>{{$l->created_at}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
