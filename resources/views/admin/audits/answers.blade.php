@extends('admin.layout')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Audits') }} <small>{{ trans('labels.ListingAllAudits') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Audits') }}</li>
        </ol>
    </section>
    

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
      
            <div class="row">
            <div class="col-md-11">

                <div class="row">
                    <div class="col-xs-12">
                        @if (count($errors) > 0)
                          @if($errors->any())
                          <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{$errors->first()}}
                          </div>
                          @endif
                        @endif
                    </div>
                </div>
                
                <div class="row">
                <div class="col-sm-3">
                  <div class="box">
                  <div class="box-body">
                  <p><strong>Overview</strong></p>
                  <p><strong>Failed Items</strong></p>
                  <p><strong>Audit</strong></p>
                    @foreach ($result['audit']->questions as $dat)
                      @if (strtolower($dat['question_type_two']) == 'header')
                      <p style="margin-left: 5px"><a href="#{{ $dat['question'] }}">{{ $dat['question'] }}</a></p>
                      @endif
                    @endforeach
                    </div>
                  </div>
                </div>
								<div class="col-sm-9">
                
                <div class="row">
                  <div class="box">
                    <div class="box-body">
                      <img src="{{ asset('images/admin_logo/Caltex-Fresh-Stop.jpg') }}" width="300px" style="margin-bottom: -50px"/>
                      <div class="col-sm-12 text-center">
                        <h3>{{ $result['audit']->heading }}</h3>
                        <!-- <label>Site: - </label> -->
                      </div>
                      <div class="col-sm-4">
                        <p>Inspection Score</p>
                        --
                      </div>
                      <div class="col-sm-4">
                        <p>Failed Items</p>
                        0
                      </div>
                    </div>
                    </div>
                  </div>
                  
                <div class="row">
                  <div class="box">
                    <div class="box-body">
                      <div class="col-sm-6">
                      <p>Site</p>
                      --
                      </div>
                      <div class="col-sm-6">
                      <p>Region</p>
                      --
                    </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="box">
                    <div class="box-body">
                      <p>Site Address</p>
                      <div id="map" style="width: 100%; height: 250px;"></div>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <?php $valid_points = 0; ?>
                  @foreach ($result['form_data'] as $dat)
                    <div class="panel panel-default">
                    @if(strtolower($dat->question_type_two) !== 'signature' && strtolower($dat->question_type) !== 'file')
                      @if (strtolower($dat->question_type_two) == 'header')
                      <div class="panel-heading">
                        <h3 id="{{ $dat->question }}">
                          {{ $dat->question . ' ('. $dat->valid_points . ' / ' . $dat->total_points .') ' }}
                          @if($dat->valid_points > 0 && $dat->total_points > 0)
                            {{ floor($dat->valid_points / $dat->total_points * 100). '%' }}
                          @endif
                        </h3 >
                      </div>
                      @endif
                      @if (strtolower($dat->question_type_two) !== 'header')
                      <div class="panel-body">
                        <p style="margin-left: 10px">{{ $dat->question }}</p>
                        @php ($dat->answer == 1 && strtolower($dat->question_type_two !== 'header')) 
                              ? $value = 'True' . ' ('.$dat->question_points.')' : $value = 'False'. ' ('.$dat->question_points.')';
                              ($dat->answer == 1 && strtolower($dat->question_type_two !== 'header')) 
                              ? $label = 'success' : $label = 'danger'
                        @endphp
                        <p style="margin-left: 10px" class="label label-{{$label}}">{{ $value }}</p>
                      </div>
                      @endif
                      @endif
                      
                     @if(strtolower($dat->question_type_two) == 'signature')
                      
                        <div class="panel-heading">
                          <!--<h3>{{ $dat->question }}</h3 >-->
                        </div>
                        <div class="panel-body">
                          <!--<img src="{{ $dat->answer }}" />-->
                        </div>
                      
                      @endif
                    </div>
                    @endforeach
                    

                </div>

@php /*
                <div class="row">
                {!! Form::open(['url' => 'admin/audits/store/' . $result['audit']->id, 'enctype'=>'multipart/form-data']) !!}
                <div class="box">
                  <div class="box-body">
                   {!! isset($result['form_data'][0]->form_data)??$result['form_data'][0]->form_data !!} 
                <div class="text ui-sortable" id="form_layout">		
                  <?php 
                    $i = 1;
                    $div = '';
                    $points = '';
                    foreach ($result['audit']->questions as $dat) {
                      $points = ($dat['question_points'] > 0)?' ('.$dat['question_points'].' points)':'';

                      $input = '';
                      $required = '';
                      $required_mark = '';

                      $op = '<option value="">--select--</option>';
                      if (!empty($dat['question'])) {
                          if($dat['question_required'] == 'yes'){
                            $required = ' required';
                            $required_mark = '<span style="color: red">*</span>';
                      }
                    
                      if(!empty($dat['question_options'])){	
                        $options = explode(",",$dat['question_options']);
                        if($dat['question_type'] == 'select box'){
                          foreach ($options as $option) {
                            $op .= '<option value="'.$option.'">'.$option.'</option>';
                          }
                        }
                      }
                      
                      if (strtolower($dat['question_type_two']) == 'header') {
                        $input = '<div class="input-container"><label class="caption">'.$dat['question'].'</label></div>';
                      }

                      if (strtolower($dat['question_type']) == 'text'  || $dat['question_type'] == 'input') {
                        $input = '<div class="input-container"><label class="caption">'.$dat['question'].'</label>
                                  <div class="input-control text full-size">
                                    <label>{textinput}</label>
                                  </div>
                                  </div>';
                      }
                      if (strtolower($dat['question_type']) == 'textarea') {
                        $input = '<div class="input-container"><label class="caption">'.$dat['question'].'</label>
                                  <div class="input-control text full-size">
                                  <label>{textinput}</label>
                                  </div>
                                  </div>';
                      }
                      if (strtolower($dat['question_type']) == 'checkbox') {
                        $input = '<div class="input-container"><label class="input-control checkbox small-check">'.$dat['question'].'</label>
                        <label>{textinput}</label>
                        </div>';
                      }
                      if (strtolower($dat['question_type']) == 'radio') {
                        $input = '<div class="input-container"><label class="input-control radio small-check">
                        '.$dat['question'].'</label><label>{textinput}</label>
                        </div>';
                      }
                      if (strtolower($dat['question_type']) == 'file') {
                        $input = '<div class="input-container"><label class="caption">'.$dat['question'].'</label>
                        
                        </div>';
                      }
                      if (strtolower($dat['question_type']) == 'select') {
                        $input = '<div class="input-container"><label class="caption">'.$dat['question'].$points.'</label>
                                  <label>{textinput}</label>
                                  </div><br/>';
                      }
                      
                      // if (strtolower($dat['question_type_two']) == 'signature') {
                      //   $input = '<div class="input-container"><label class="caption">'.$dat['question'].$points.'</label>
                      //         <div class="input-control select full-size" style="height: 100px">
                      //           <canvas name="{names}" width="200" height="100" style="background: gainsboro">
                      //         </canvas>
                      //       </div>
                      //     </div>';
                      // }
                      
                      $value = '';
                      foreach($result['audit']->answers as $a) {    
                      if($dat['question'] == $a->field){
                      $value = $a->value;
                      }
                    }

                      $form = str_replace('{textinput}',$value,$input);
                      // $form2 = str_replace('{required}',$required,$form);
                      $form3 = str_replace('{options}',$op,$form);
                      // $form4 = str_replace('{names}','question_'.$i.'_'.$dat['id'],$form3);
                      $form4 = str_replace('{names}',$dat['question'],$form3);
                      $div .= $form4;

                    }
                    $i++;
                  }
                  
                        // $div .= '<input type="submit" value="submit" class="btn btn-primary btn-sm" style="position: relative; left: 15px;" />';
                    echo $div;

                    
                    ?>
                </div>
              </div>
              
              {!! Form::close() !!}
            </div>
            
            <div class="row">
              <p>Conclusion</p>
              <div class="box">
                <div class="box-body">
                  ---
                </div>
              </div>
              <div class="col-sm-12">
                <div class="box">
                  <div class="box-body">
                    <p>Retail / Manager on Duty</p>
                    @foreach ($result['audit']->questions as $dat)
                    @if (strtolower($dat['question_type_two']) == 'signature')
                    <!-- <img src=""/> -->
                    @endif
                    <canvas name="{names}" width="200" height="100" style="background: gainsboro">
                      @endforeach
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="box">
                    <div class="box-body">
                      <p>Name & Surname of Auditor</p>
                      @foreach ($result['audit']->questions as $dat)
                      @if (strtolower($dat['question_type_two']) == 'signature')
                      <!-- <img src=""/> -->
                      @endif
                      <canvas name="{names}" width="200px" height="100px" style="background: gainsboro">
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
    */ @endphp      
              
              
            </section>
          </div>
          <script src="{{ asset('js2/app.js') }}"></script>
          @endsection
          @section('script')
          
          <script>
  //console.log($("[name='<?php // echo $result['answers'][0]->field;?>']").val());
</script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script>
			var map;
			function initialize() {
				var myLatlng = new google.maps.LatLng(-33.946978, 18.649641,17);
				var mapOptions = {
					zoom: 16,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					scrollwheel: false  
				};
				
				map = new google.maps.Map(document.getElementById('map'), mapOptions);
				
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'Site'
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);

		</script>

@endsection