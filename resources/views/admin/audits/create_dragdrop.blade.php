@extends('admin.layout')

@section('content')
	<link href="{{ asset('css2/app.css') }}" rel="stylesheet">
	<link href="{{ asset('metro/css/metro.css')  }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('metro/css/metro-icons.css')  }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('metro/css/metro-responsive.css')  }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('metro/css/metro-schemes.css')  }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('metro/css/font-awesome.css')  }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('metro/plugins/bootstrap/css/bootstrap.metro.css')  }}" rel="stylesheet" type="text/css"/>

	{!! SiteHelpers::call_css(isset($result['css'])?$result['css']:array()) !!}
		<link href="{{ asset('metro/css/docs.css')  }}" rel="stylesheet" type="text/css"/>
    <!--<link href="{{ asset('metro/css/docs-rtl.css')  }}" rel="stylesheet" type="text/css"/>-->
     
    <script src="{{ asset('metro/plugins/tabbehave/behave.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/tabbehave/behave.helper.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/jquery-2.1.3.min.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/metro.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/bootstrap/js/bootstrap.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/jquery-ui.min.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/parsley.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/nanoscroller/js/jquery.nanoscroller.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/jquery.jCombo.min.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/select2.min.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/preCode.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/jquery.form.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/fancybox/jquery.fancybox.pack.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/countup/countUp.min.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/plugins/jquery-form/jquery.form.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/wow.min.js')  }}" type="text/javascript" ></script>
    <script src="{{ asset('metro/js/web.js')  }}" type="text/javascript" ></script>

		{!! SiteHelpers::call_js(isset($result['js'])?$result['js']:array()) !!}
	
    <script src="{{ asset('metro/js/docs.js')  }}" type="text/javascript" ></script>

<style>
#form_layout {position: relative;}	
.fluent-menu .input-control {margin:3px 0;line-height: 1.5rem;height: 1.5rem;}
.fluent-menu .active-button {background-color: #BCDDFA;}
.color-box {border:1px solid #000;width: 1.25rem;height:1rem;float: left;margin:1px;}
#zen_input {height: 85px;min-height: 85px;width: 200px;}
#zen_output {height: 85px;min-height: 85px;width: 500px;}
.sub-class .d-menu {width: 15.625rem;}
.dropdown-wrap {position: relative;}
.fluent-menu .input-control.text button.button {height:1.5rem;padding:0.25rem 0.375rem;line-height: 0.8rem; }
.fluent-menu .tabs-content {z-index: 0;}

.popover {border:1px solid rgba(0,0,0,0.6);}
.popover:before {border-left: 1px solid rgba(0,0,0,0.6);border-bottom: 1px solid rgba(0,0,0,0.6);}
#formbuilder_property label {text-transform: capitalize;}
#formbuilder_property h4:first-child {margin-top:0;font-weight: bold;border-bottom: 1px solid #bbb;box-shadow: 0px 1px 0px #fff;padding-bottom:8px;}

#formbuilder_property {
	position: absolute !important;
	/* top: 100px !important; */
	left: 600px !important;
	display: block !important;
	width: 350px !important;
	/* background-color: coral !important; */
}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Audits') }} <small>{{ trans('labels.ListingAllAudits') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Audits') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

      
	<div class="panel widget-box ">
		<div class="heading">
			<div class="title">Visual Form Builder</div>
		</div>
		<div class="content">
		
			<div class="text" >
				
				<div class="fluent-menu" data-role="fluentmenu">
					<ul class="tabs-holder">
						<li class="active"><a href="#basic">Metro</a></li>
						<li><a href="#code">Code</a></li>
					</ul>

					<div class="tabs-content">

						<div class="tab-panel" id="basic">
							<div class="tab-panel-group">
								<div class="tab-group-content">
									<button class="fluent-button" id="add_header">
										<span class="icon fa fa-header"></span>
										<span>Header</span>
									</button>
									<button class="fluent-button" id="add_text">
										<span class="icon fa fa-edit"></span>
										<span>Text</span>
									</button>
									<button class="fluent-button" id="add_textarea">
										<span class="icon fa fa-edit"></span>
										<span>Textarea</span>
									</button>
								</div>
								<div class="tab-group-caption">Text</div>
							</div>
							<div class="tab-panel-group">
								<div class="tab-group-content">
									<button class="fluent-button" id="add_checkbox">
										<span class="icon fa fa-check-square-o"></span>
										<span>Checkbox</span>
									</button>
									<button class="fluent-button" id="add_radio">
										<span class="icon fa fa-dot-circle-o"></span>
										<span>Radio</span>
									</button>
									<!-- <button class="fluent-button" id="add_switch">
										<span class="icon fa fa-toggle-on"></span>
										<span>Switch</span>
									</button> -->
									<button class="fluent-button" id="add_signature">
										<span class="icon fa fa-pencil"></span>
										<span>Signature</span>
									</button>
								</div>
								<div class="tab-group-caption">Option</div>
							</div>
							<div class="tab-panel-group">
								<div class="tab-group-content">
									<button class="fluent-button" id="add_select">
										<span class="icon fa fa-angle-down"></span>
										<span>Select</span>
									</button>
									<button class="fluent-button" id="add_file">
										<span class="icon fa fa-upload"></span>
										<span>File</span>
									</button>
									<button class="fluent-button" id="add_button">
										<span class="icon fa fa-toggle-right"></span>
										<span>Button</span>
									</button>

								</div>
								<div class="tab-group-caption">More</div>
							</div>

						</div>

						
						<!-- code -->
						<div class="tab-panel" id="code">
							<div class="tab-panel-group">
								<div class="tab-group-content">
									<button class="fluent-big-button" id="show_code">
										<span class="icon fa fa-eye"></span>
										<span>Show / Hide<br/>Code</span>
									</button>
									<button class="fluent-big-button" id="build_code">
										<span class="icon fa fa-cloud-download"></span>
										<span>Build<br/>Code</span>
									</button>
								</div>
								<div class="tab-group-caption">Code</div>
							</div>
							<div class="tab-panel-group">
								<div class="tab-group-content">

									<div class="tab-content-segment">
										<button class="fluent-button" >
											<div class="input-control textarea">
												<textarea name="" id="zen_input" placeholder="try table.table>tr>th{heading}*3"
												></textarea>
											</div>
										</button>
									</div>
									<div class="tab-content-segment">
										<button class="fluent-button" >
											<div class="input-control textarea">
												<textarea name="" id="zen_output" 
												></textarea>
											</div>
										</button>
									</div>

								</div>
								<div class="tab-group-caption">Zen Code Tools</div>
							</div>
						</div>

					</div>
				</div>

				<div id="form_preview" class="grid bg-grayLight padding10 bg-chess" style="min-height: 250px;">
					<div class="row cells3">
						<div class="cell colspan2">
							
							<div class="panel widget-box">
								<div class="heading">
									<div class="title">Form Builder</div>
								</div>
								<div class="content">
									
									<div class="row">

										<div class="col-md-5 input-container">
												<label class="caption">Audit Heading</label>
													<div class="input-control text full-size">
															<input type="text" data-form-builder="text" name="audit_heading" id="audit_heading" value="">
													</div>
										</div>

										<div class="col-md-3 input-container">
												<label class="caption">Audit Type</label>
													<div class="input-control text full-size">
													<select name="audit_type" id="audit_type"> 
																<option value="contact">Contact Report</option>
																<option value="food">Food Report</option>
																<option value="snapshot">Snapshot</option>
																<option value="weekday">Weekday</option>
																<option value="weekend">Weekend</option>
															</select>
												</div>
										</div>

										<div class="col-md-2 input-container">
												<label class="caption">Region</label>
													<div class="input-control text full-size">
														<select name="region" id="audit_region"> 
																<option value="EC">EC</option>
																<option value="WC">WC</option>
																<option value="KZN">KZN</option>
																<option value="Inland">Inland</option>
																<option value="National">National</option>
															</select>
													</div>
										</div>

										<div class="col-md-2 input-container">
												<label class="caption">Status</label>
													<div class="input-control text full-size">
														<select name="status" id="audit_status"> 
																<option value="1">Active</option>
																<option value="0">Inactive</option>
															</select>
													</div>
										</div>

									</div>

									<div class="text" id="form_layout">
										
									</div>
								</div>
							</div>

						</div>
						<div class="cell"></div>
					</div>
				</div>
				<div class="input-control full-size textarea hide" id="code_build">
					<label>Generated Code</label>
					{!! Form::open(['url' => 'admin/audits/generate_builder','target' => "_blank", 'id' => 'builderForm']) !!}
					<textarea name="builder_coder" id="text_code" cols="30" rows="10"></textarea>
					<input type="text" class="form-control" name="audit_heading" id="audit_heading_hidden" />
					<input type="text" class="form-control" name="audit_type" id="audit_type_hidden" />
					<input type="text" class="form-control" name="region" id="audit_region_hidden" />
					<input type="text" class="form-control" name="status" id="audit_status_hidden" />
					<div name="builder_coder" id="text_code2" style="display:none"></div>
					<input type="submit" value="Submit" />
					{!! Form::close() !!}
				</div>




			</div>
		</div>

<div class="hide" id="zen_area"></div>
    </section>
</div>

<script type='text/javascript' >
      jQuery(document).ready(function($){
        <?php   
          
          $notify = Session::get('notify');
          if( Session::has('message') ){
            $notify['caption'] = Session::get('message');
            $notify['type'] = 'info';
          }
        ?>
        $.Notify({
          content: '<?php echo $notify['caption'];?>',
          type: '<?php echo $notify['type'] ?>',
          shadow: true
        })
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
      })
    </script>




<script src="{{ asset('js2/app.js') }}"></script>
@endsection