@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            <small>{{ trans('labels.title_dashboard') }} {{$result['commonContent']['setting']['admin_version']}}</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</li>
            </ol>
        </section>
       
        <!-- Main content -->
        <section class="content" style="height: 90vh;">
            @if( $result['commonContent']['roles'] != null and $result['commonContent']['roles']->dashboard_view == 1)
            <div class="col-md-12" style="overflow: auto; height: 100%;">
            @foreach($result['retailers'] as $key => $retailer)   
                <div class="panel">
                    <!-- <div class="panel-heading text-center"><h3>{{ $key }}</h3></div> -->
                    <div class="panel-body">
                        <table class="table table-bordered table-sm">
                            <thead>
                              <tr>
                                <th colspan="3"><h3>PREMISE INSPECTION</h3></th>
                                <th colspan="3">Quater 1</th>
                                <th colspan="3">Quater 4</th>
                              </tr>
                              <tr>
                                <th>Region</th>
                                <th>Division</th>
                                <th>Store Name</th>
                                <th>May20</th>
                                <th>April20</th>
                                <th>Mar20</th>
                                <th>Dec19</th>
                                <th>Jan20</th>
                                <th>Feb20</th>
                              </tr>
                            </thead>    
                            <tbody>
                            @foreach($retailer as $bc)
                                <tr>
                                    <td>{{ $bc->area }}</td>
                                    <td>''</td>
                                    <td>{{ $bc->site_name }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </section>
        <!-- /.content -->
    </div>
    <script src="{!! asset('admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>

    <script src="{!! asset('admin/dist/js/pages/dashboard2.js') !!}"></script>
@endsection
