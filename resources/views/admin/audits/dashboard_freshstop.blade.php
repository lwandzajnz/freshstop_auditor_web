@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            <small>{{ trans('labels.title_dashboard') }} {{$result['commonContent']['setting']['admin_version']}}</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</li>
            </ol>
        </section>
       
        <!-- Main content -->
        <section class="content" style="height: 90vh;">
            @if( $result['commonContent']['roles'] != null and $result['commonContent']['roles']->dashboard_view == 1)
            <h3 style="margin-top: -10px;">Team Dashboard</h3>
            <div class="col-md-12" style="overflow: auto; height: 100%;">
            @foreach($result['new'] as $key => $bc)   
                <div class="panel">
                    <!-- <div class="panel-heading text-center"><h3>{{ $key }}</h3></div> -->
                    <div class="panel-body">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <th width="300px">{{ $key }}</th>
                                <th>#Stores</th>
                                <th>Weekend</th>
                                <th>Snapshot</th>
                                <th>Action/Contact</th>
                                <th>Food Service</th>
                                <th>Cashier</th>
                                <th>Avg Cashier</th>
                                <th>KPI SCORE</th>
                            </thead>    
                            <tbody>
                            @foreach($bc as $s)
                           
                                <tr>
                                    <td width="300px"><a href="{{ URL::to('admin/snapshot/dashboard/'.$s->id) }}" style="color: #000">{{ $s->name }}</a></td>
                                    <td><a href="{{ URL::to('admin/snapshot/dashboard/'.$s->id) }}" style="color: #000">{{ $s->total }}</a></td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">
                                        <a href="{{ URL::to('admin/snapshot/dashboard/'.$s->id) }}" style="color: #000">{{ $rand }}%</a>
                                    </td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                </tr>
                              
                                @endforeach
                            <tr>
                                <td><strong>AVERAGE</strong></td>
                                <td></td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                                <?php $rand = rand(0,100);?>
                                <td style="background: <?php echo ($rand >= 80 ? '#7EDF7B':'#F55E5E');?>">{{ $rand }}%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </section>
        <!-- /.content -->
    </div>
    <script src="{!! asset('admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>

    <script src="{!! asset('admin/dist/js/pages/dashboard2.js') !!}"></script>
@endsection
