@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            <small>{{ trans('labels.title_dashboard') }} {{$result['commonContent']['setting']['admin_version']}}</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</li>
            </ol>
        </section>
       
        <!-- Main content -->
        <section class="content">
            @if( $result['commonContent']['roles'] != null and $result['commonContent']['roles']->dashboard_view == 1)
            <h3 style="margin-top: -10px;">Team Dashboard</h3>
            <div class="col-md-12" style="overflow: auto; height: 100%;">
            @foreach($result['retailers'] as $key => $bc)   
                <div class="panel">
                    <!-- <div class="panel-heading text-center"><h3>{{ $key }}</h3></div> -->
                    <div class="panel-body">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <th width="100px">Region</th>
                                <th width="250px">Store Name</th>
                                <th>Weekday</th>
                                <th>Weekend</th>
                                <th>Snapshot</th>
                                <th>Action/Contact</th>
                                <th>Food Service</th>
                                <th>Cashier</th>
                                <th>Avg Cashier</th>
                                <th>KPI SCORE</th>
                            </thead>    
                            <tbody>
                            @foreach($bc as $ukey => $value)
                              <tr style="background-color: #063010; color: #DDD">
                                <th>{{ $value[0]->site->user->first_name }} </th>
                                <th>&nbsp;</th>
                                <th>Weekday</th>
                                <th>Weekend</th>
                                <th>Snapshot</th>
                                <th>Action/Contact</th>
                                <th>Food Service</th>
                                <th>Cashier</th>
                                <th>Avg Cashier</th>
                                <th>KPI SCORE</th>
                              </tr>
                              @foreach($value as $s)
                                <tr>
                                    <td width="150px">{{ $s->site->site->area }}</td>
                                    <td>{{ $s->site->site->site_name }}</td>
                                    
                                    <td style="background: <?php echo (isset($s->stats->total_percentage) && (strtolower($s->site->audit->audit_type) == "weekday") && ($s->stats->total_percentage >= 1)) ? '#7EDF7B':'#F55E5E';?>;">
                                        @if(strtolower($s->site->audit->audit_type) == "weekday")
                                            <!--<a style="color: #FFF;" href="{{ URL::to('admin/snapshot/weekend/'.$s->site->user->id.'/'.$s->site->site->id) }}">{{ $s->stats->total_percentage ?? 0 }}%</a>-->
                                            <a style="color: #000;" href="{{ URL::to('admin/snapshot/report/weekday/'.$s->site->id) }}">{{ $s->stats->total_percentage ?? 0 }}%</a>
                                        @else
                                            0%
                                        @endif
                                    </td>
                                     <td style="background:  <?php echo (isset($s->stats->total_percentage) && (strtolower($s->site->audit->audit_type) == "weekend") && ($s->stats->total_percentage >= 1)) ? '#7EDF7B':'#F55E5E';?>;">
                                        @if(strtolower($s->site->audit->audit_type) == "weekend")
                                            <!--<a style="color: #FFF;" href="{{ URL::to('admin/snapshot/weekend/'.$s->site->user->id.'/'.$s->site->site->id) }}">{{ $s->stats->total_percentage ??0 }}%</a>-->
                                            <a style="color: #000;" href="{{ URL::to('admin/snapshot/report/weekend/'.$s->site->id) }}">{{ $s->stats->total_percentage ?? 0 }}%</a>
                                        @else
                                            0%
                                        @endif
                                    </td>
                                    
                                    
                                    <td style="background:  <?php echo (isset($s->stats->total_percentage) && (strtolower($s->site->audit->audit_type) == "snapshot") && ($s->stats->total_percentage >= 0)) ? '#7EDF7B':'#F55E5E';?>;">
                                        @if($s->site->audit->audit_type == "snapshot")
                                        <!--<a style="color: #FFF;" href="{{ URL::to('admin/snapshot/weekend/'.$s->site->user->id.'/'.$s->site->site->id) }}">{{ $s->stats->total_percentage ??0 }}%</a>-->
                                        <a style="color: #000;" href="{{ URL::to('admin/snapshot/report/snapshot/'.$s->site->id) }}">{{ $s->stats->total_percentage ?? 0 }}%</a>
                                        @else
                                            0%
                                        @endif
                                    </td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background:  <?php echo (isset($s->stats->total_percentage) && (strtolower($s->site->audit->audit_type) == "contact") && ($s->stats->total_percentage >= 0)) ? '#7EDF7B':'#F55E5E';?>;">
                                         @if(strtolower($s->site->audit->audit_type) == "contact")
                                            <!--<a style="color: #FFF;" href="{{ URL::to('admin/snapshot/contact/'.$s->site->user->id.'/'.$s->site->site->id) }}">{{ $s->stats->total_percentage ??0 }}%</a>-->
                                            <a style="color: #000;" href="{{ URL::to('admin/snapshot/report/contact/'.$s->site->id) }}">{{ $s->stats->total_percentage ?? 0 }}%</a>
                                        @else
                                            0%
                                        @endif</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background:  <?php echo (isset($s->stats->total_percentage) && (strtolower($s->site->audit->audit_type) == "food") && ($s->stats->total_percentage >= 1)) ? '#7EDF7B':'#F55E5E';?>;">
                                         @if($s->site->audit->audit_type == "food")
                                        <!--<a style="color: #FFF;" href="{{ URL::to('admin/snapshot/weekend/'.$s->site->user->id.'/'.$s->site->site->id) }}">{{ $s->stats->total_percentage ??0 }}%</a>-->
                                        <a style="color: #000;" href="{{ URL::to('admin/snapshot/report/food/'.$s->site->id) }}">{{ $s->stats->total_percentage ?? 0 }}%</a>
                                        @else
                                            0%
                                        @endif</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 101 ? '#7EDF7B':'#F55E5E');?>; color: #000">{{ 0 }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 101 ? '#7EDF7B':'#F55E5E');?>; color: #000">{{ 0 }}%</td>
                                    <?php $rand = rand(0,100);?>
                                    <td style="background: <?php echo ($rand >= 101 ? '#7EDF7B':'#F55E5E');?>; color: #000">{{ 0 }}%</td>
                                </tr>
                              
                              @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </section>
        <!-- /.content -->
    </div>
    <script src="{!! asset('admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>

    <script src="{!! asset('admin/dist/js/pages/dashboard2.js') !!}"></script>
@endsection
