@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Audits') }} <small>{{ trans('labels.ListingAllAudits') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Audits') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
              <div class="box">

          

              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        @if (count($errors) > 0)
                          @if($errors->any())
                          <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{$errors->first()}}
                          </div>
                          @endif
                        @endif
                    </div>
                </div>
                
               
                <div class="row">
                  <div class="col-sm-12 text-center">
                    <h3>{{ $result['audit']->heading }}</h3>
                    <label>Site: - </label>
									</div>
									
                </div>
                <div class="row">
								@php /*
								<div class="col-sm-12">
								{!! Form::open(['url' => 'admin/audits/store/' . $result['audit']->id, 'enctype'=>'multipart/form-data']) !!}
							
                 <?php echo $result['form_data'][0]->form_data; ?>

								 <input type="submit" value="submit" class="btn btn-primary" />
								{!! Form::close() !!}
								</div>
								*/ @endphp
                <?php 
		   $i = 1;
			$div = '<table class="table table-bordered">';
		   foreach ($result['audit']->questions as $dat) {
		
			   $input = '';
			$required = '';
				$required_mark = '';
			$op = '<option value="">--select--</option>';
			if (!empty($dat['question'])) {
			    if($dat['question_required'] == 'yes'){
						$required = ' required';
						$required_mark = '<span style="color: red">*</span>';
					}
					
				$div .= '<tr><td width="500px"><strong>'.$dat['question_number'].')  '.$dat['question'].': </strong>'.$required_mark.'</td>';
			
				
				if(!empty($dat['question_options'])){	
					$options = explode(",",$dat['question_options']);
					if($dat['question_type'] == 'select box'){
						foreach ($options as $option) {
							$op .= '<option value="'.$option.'">'.$option.'</option>';
						}
					}
				}

				if(strtolower($dat['question_type']) == 'text'){
					$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input>';
				}
				if(strtolower($dat['question_type']) == 'short text'){
					$input = '<input class="form-control" name="{names}" value="{textinput}" {required} ></input>';
				}
				if(strtolower($dat['question_type']) == 'textarea'){
					$input = '<textarea class="form-control" name="{names}" {required} >{textinput}</textarea>';
				}
				if(strtolower($dat['question_type']) == 'select box'){
						$input = '<select class="form-control" name="{names}" {required} >{options}</select>';
				}
				if(strtolower($dat['question_type']) == 'checkbox'){
						$check_options = explode(",",$dat['question_options']);
						foreach($check_options as $co){
							$input .= '<label>'.$co.'</label>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="" name="{names}[]" {required} value="'.$co.'" /><br/>';
						}
				}
				if(strtolower($dat['question_type']) == 'radio'){
						$radio_options = explode(",",$dat['question_options']);
						foreach($radio_options as $ro){
							$input .= '<label>'.$ro.'</label>&nbsp;&nbsp;&nbsp;<input type="radio" class="" name="{names}" {required} value="'.$ro.'" /><br/>';
						}
				}
			
				$form = str_replace('{textinput}',"",$input);
				$form2 = str_replace('{required}',$required,$form);
				$form3 = str_replace('{options}',$op,$form2);
				$form4 = str_replace('{names}','question_'.$i.'_'.$dat['id'],$form3);
					
				$div .= '<td width="">'.$form4.'</td>
				</tr>';
			
			   }
			$i++;
			
		}
          $div .= '</table><input type="submit" value="submit" class="btn btn-primary btn-sm" style="position: relative; left: 15px;" />';
		  echo $div;
?>

        

                </div>
            </div>
        </div>
    </section>
</div>

@endsection
@section('script')

<script>
  //console.log($("[name='<?php // echo $result['answers'][0]->field;?>']").val());
</script>
@endsection