@extends('admin.layout')
@section('content')
<!-- <link href="{{ asset('css2/app.css') }}" rel="stylesheet"> -->
<link href="{{ asset('metro/css/metro.css')  }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('metro/css/metro-icons.css')  }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('metro/css/metro-responsive.css')  }}" rel="stylesheet" type="text/css"/>
    <!-- <link href="{{ asset('metro/css/metro-schemes.css')  }}" rel="stylesheet" type="text/css"/> -->
    <!-- <link href="{{ asset('metro/css/font-awesome.css')  }}" rel="stylesheet" type="text/css"/> -->
    <!-- <link href="{{ asset('metro/plugins/bootstrap/css/bootstrap.metro.css')  }}" rel="stylesheet" type="text/css"/> -->
    <!-- <link href="{{ asset('metro/css/docs.css')  }}" rel="stylesheet" type="text/css"/> -->
    <!-- <link href="{{ asset('metro/css/docs-rtl.css')  }}" rel="stylesheet" type="text/css"/> -->	
    <!-- <script src="{{ asset('metro/js/docs.js')  }}" type="text/javascript" ></script> -->

<style>
#form_layout {position: relative;}	
.fluent-menu .input-control {margin:3px 0;line-height: 1.5rem;height: 1.5rem;}
.fluent-menu .active-button {background-color: #BCDDFA;}
.color-box {border:1px solid #000;width: 1.25rem;height:1rem;float: left;margin:1px;}
#zen_input {height: 85px;min-height: 85px;width: 200px;}
#zen_output {height: 85px;min-height: 85px;width: 500px;}
.sub-class .d-menu {width: 15.625rem;}
.dropdown-wrap {position: relative;}
.fluent-menu .input-control.text button.button {height:1.5rem;padding:0.25rem 0.375rem;line-height: 0.8rem; }
.fluent-menu .tabs-content {z-index: 0;}

.popover {border:1px solid rgba(0,0,0,0.6);}
.popover:before {border-left: 1px solid rgba(0,0,0,0.6);border-bottom: 1px solid rgba(0,0,0,0.6);}
#formbuilder_property label {text-transform: capitalize;}
#formbuilder_property h4:first-child {margin-top:0;font-weight: bold;border-bottom: 1px solid #bbb;box-shadow: 0px 1px 0px #fff;padding-bottom:8px;}
.input-control textarea{
  min-height: 0 !important;
  /* height: auto !important; */
}
#formbuilder_property {
	position: absolute !important;
	/* top: 100px !important; */
	left: 600px !important;
	display: block !important;
	width: 350px !important;
	/* background-color: coral !important; */
}

</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Audits') }} <small>{{ trans('labels.ListingAllAudits') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Audits') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
              <div class="box">

          

              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        @if (count($errors) > 0)
                          @if($errors->any())
                          <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{$errors->first()}}
                          </div>
                          @endif
                        @endif
                    </div>
                </div>
                
               
                <div class="row">
                  <div class="col-sm-12 text-center">
                    <h3>{{ $result['audit']->heading }}</h3>
                    <label>Site: - </label>
									</div>
									
                </div>
                <div class="row">
								<div class="col-sm-10">
								
								{!! Form::open(['url' => 'admin/audits/store/' . $result['audit']->id, 'enctype'=>'multipart/form-data']) !!}
							
                 <?php echo $result['form_data'][0]->form_data; ?>

								 <input type="submit" value="submit" class="btn btn-primary" />
								{!! Form::close() !!}
								

  @php /*
  {!! Form::open(['url' => 'admin/audits/store/' . $result['audit']->id, 'enctype'=>'multipart/form-data']) !!}
  <div class="text ui-sortable" id="form_layout">		
      <?php 
		   $i = 1;
      $div = '';
      $points = '';
      foreach ($result['audit']->questions as $dat) {
        $points = ($dat['question_points'] > 0)?' ('.$dat['question_points'].' points)':'';

        $input = '';
        $required = '';
          $required_mark = '';
        $op = '<option value="">--select--</option>';
        if (!empty($dat['question'])) {
            if($dat['question_required'] == 'yes'){
              $required = ' required';
              $required_mark = '<span style="color: red">*</span>';
        }
					
				// $div .= '';
			
				if(!empty($dat['question_options'])){	
					$options = explode(",",$dat['question_options']);
					if($dat['question_type'] == 'select box'){
						foreach ($options as $option) {
							$op .= '<option value="'.$option.'">'.$option.'</option>';
						}
					}
				}

        if (strtolower($dat['question_type']) == 'header') {
					$input = '<div class="input-container"><label class="caption">'.$dat['question'].'</label></div>';
        }

        if (strtolower($dat['question_type']) == 'text' || $dat['question_type'] == 'input') {
					$input = '<div class="input-container"><label class="caption">'.$dat['question'].'</label>
                    <div class="input-control text full-size">
                      <input type="text" data-form-builder="text" name="{names}" value="{textinput}">
                    </div>
                    </div>';
        }
        if (strtolower($dat['question_type']) == 'textarea') {
					$input = '<div class="input-container"><label class="caption">'.$dat['question']. $points.'</label>
                    <div class="input-control text full-size">
                     <textarea name="{names}" value="{textinput}" rows="5"></textarea>
                    </div>
                    </div>';
        }
        if (strtolower($dat['question_type']) == 'checkbox') {
					$input = '<div class="input-container"><label class="input-control checkbox small-check">
            <input type="checkbox" name="{names}" data-form-builder="checkbox" value="">
            <span class="check"></span><span class="caption">'.$dat['question'].$points.'</span>
          </label>
          </div>';
        }
        if (strtolower($dat['question_type']) == 'radio') {
					$input = '<div class="input-container"><label class="input-control radio small-check">
          <input type="radio" name="{names}" data-form-builder="radio"><span class="check"></span><span class="caption">'.$dat['question'].$points.'</span></label>
          </div>';
        }
        if (strtolower($dat['question_type']) == 'file') {
					$input = '<div class="input-container"><label class="caption">'.$dat['question'].$points.'</label>
          <div class="input-control file full-size" data-role="input">
          <input type="file" name="{names}" class="form-control" style="width: 100%">
          <button class="button" data-form-builder="file" type="button"><span class="mif-folder"></span></button>
          </div>
          </div>';
        }
        if (strtolower($dat['question_type']) == 'select') {
					$input = '<div class="input-container"><label class="caption">'.$dat['question'].$points.'</label>
                      <div class="input-control select full-size">
                          <select name="select" data-form-builder="select">
                              <option value="">-Select One-</option>
                              <option value="value">option</option>
                              <option value="value">option</option>
                          </select>
                      </div>
                    </div>';
        }
        if (strtolower($dat['question_type']) == 'signature') {
          $input = '<div class="input-container"><label class="caption">'.$dat['question'].$points.'</label>
              <div class="input-control select full-size" style="height: 100px">
                 <canvas name="{names}" width="200" height="100" style="background: gainsboro">
                 </canvas>
              </div>
            </div>';
        }
        $form = str_replace('{textinput}',"",$input);
				// $form2 = str_replace('{required}',$required,$form);
				$form3 = str_replace('{options}',$op,$form);
				// $form4 = str_replace('{names}','question_'.$i.'_'.$dat['id'],$form3);
				$form4 = str_replace('{names}',$dat['question'],$form3);
        $div .= $form4;

			}
			$i++;
			
		}
          $div .= '<input type="submit" value="submit" class="btn btn-primary btn-sm" style="position: relative; left: 15px;" />';
		  echo $div;


?>
</div>
	{!! Form::close() !!}
  */ @endphp
                </div>
            </div>
        </div>
    </section>
</div>
<script src="{{ asset('js2/app.js') }}"></script>
@endsection
@section('script')

<script>
  //console.log($("[name='<?php // echo $result['answers'][0]->field;?>']").val());
</script>
@endsection