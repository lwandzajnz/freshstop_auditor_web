@extends('admin.layout')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            <small>{{ trans('labels.title_dashboard') }} {{$result['commonContent']['setting']['admin_version']}}</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</li>
            </ol>
        </section>
       
        <!-- Main content -->
        <section class="content">
            @if( $result['commonContent']['roles'] != null and $result['commonContent']['roles']->dashboard_view == 1)
            @foreach($result['organizations'] as $organization)   
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-heading text-center"><h3>{{ $organization->title }}</h3></div>
                    <div class="panel-body">
                        @foreach($organization->categories as $category)
                            @if($category->categories_id <= 2)
                            <div class="form-group">
                             <img src="{{ asset($category->path) }}" width="25px" /> <a href="{{ URL::to('admin/audits/dashboard',[$category->categories_id,$organization->title]) }}">{{ $category->categories_name }}</a>
                            </div>
                            @else
                            <div class="form-group">
                             <img src="{{ asset($category->path) }}" width="25px" /> <a href="{{ URL::to('admin/documents/categories',$category->categories_id) }}">{{ $category->categories_name }}</a>
                            </div>
                           @endif
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </section>
        <!-- /.content -->
    </div>
    <script src="{!! asset('admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>

    <script src="{!! asset('admin/dist/js/pages/dashboard2.js') !!}"></script>
@endsection
