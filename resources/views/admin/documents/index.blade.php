@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Documents') }} <small>{{ trans('labels.ListingAllDocuments') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Documents') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-6 form-inline" id="contact-form">
                                    <form name='registration' id="registration" class="registration" method="get" action="{{url('admin/documents/filter')}}">
                                        <input type="hidden" value="{{csrf_token()}}">
                                        {{--<div class="input-group-btn search-panel ">--}}
                                        <div class="input-group-form search-panel ">
                                            <select type="button" class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown" name="FilterBy" id="FilterBy">
                                                <option value="" selected disabled hidden>Filter By</option>
                                                <option value="Category" @if(isset($filter)) @if ($filter=="Category" ) {{ 'selected' }} @endif @endif>{{ trans('labels.Category') }}</option>
                                            </select>
                                            <input type="text" class="form-control input-group-form " name="parameter" placeholder="Search term..." id="parameter" @if(isset($parameter)) value="{{$parameter}}" @endif>
                                            <button class="btn btn-primary " id="submit" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                            @if(isset($parameter,$filter)) <a class="btn btn-danger " href="{{url('admin/customers/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                        </div>
                                    </form>
                                    <div class="col-lg-4 form-inline" id="contact-form12"></div>
                                </div>
                                <div class="box-tools pull-right">
                                    <!-- <a href="{{ url('admin/documents/add')}}" type="button" class="btn btn-block btn-primary">{{ trans('labels.AddNew') }}</a> -->
                                    <a data-toggle="modal" data-target="#addNewDocumentModal" class="btn btn-block btn-primary">{{ trans('labels.AddNew') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (count($errors) > 0)
                                  @if($errors->any())
                                  <div class="alert alert-success alert-dismissible" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      {{$errors->first()}}
                                  </div>
                                  @endif
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Filename</th>
                                            <th width="50px">{{ trans('labels.Action') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (isset($result['documents']))
                                            @foreach ($result['documents'] as $documents)
                                            <tr>
                                                <td width="175px">{{ $documents->category->categories_slug }}</td>
                                                <td><a href="{{ URL::to('admin/documents/document',$documents->id) }}">{{ $documents->file_name }}</a></td>
                                                <!-- <td><a href="{{ URL::to('https://docs.google.com/viewerng/viewer?url='.$documents->document_path) }}">{{ $documents->file_name }}</a></td> -->
                                                <td>
                                                <form method="POST" action="{{ URL::to('admin/documents/document', $documents->id) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?');"><i class="fa fa-trash"></i></button>
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @else
                                        <tr>
                                            <td colspan="4">{{ trans('labels.NoRecordFound') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                @if (count($result['documents']) > 0)
                                <div class="col-xs-12 text-right">
                                    {{$result['documents']->links()}}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>

        <!-- /.row -->

     
    <div class="modal fade" id="addNewDocumentModal" tabindex="-1" role="dialog" aria-labelledby="addNewDocumentModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content documentContent">
                {!! Form::open(array('url' => 'admin/documents/store', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body" style="padding: 25px">    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="file" class="form-control" name="file" required />
                            </div>
                        </div>
               
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 col-md-2 control-label">{{ trans('labels.Category') }}<span style="color:red;">*</span></label>
                            <div class="col-sm-10 col-md-9">
                                <?php print_r($result['categories']); ?>
                                <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                    {{ trans('labels.ChooseCatgoryText') }}.</span>
                                <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                            </div>
                        </div>
                    </div>
         
               
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-sm pull-right" value="Upload" />
                        </div>
                    </div>
                </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    </section>
    <!-- /.content -->
</div>

@endsection
