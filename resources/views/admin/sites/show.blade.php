@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Sites') }} <small>{{ trans('labels.ListingAllSites') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Sites') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-6 form-inline" id="contact-form">
                                    <form name='registration' id="registration" class="registration" method="get" action="{{url('admin/audits/filter')}}">
                                        <input type="hidden" value="{{csrf_token()}}">
                                        {{--<div class="input-group-btn search-panel ">--}}
                                        <div class="input-group-form search-panel ">
                                            <select type="button" class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown" name="FilterBy" id="FilterBy">
                                                <option value="" selected disabled hidden>Filter By</option>
                                                <option value="Category" @if(isset($filter)) @if ($filter=="Category" ) {{ 'selected' }} @endif @endif>{{ trans('labels.Category') }}</option>
                                            </select>
                                            <input type="text" class="form-control input-group-form " name="parameter" placeholder="Search term..." id="parameter" @if(isset($parameter)) value="{{$parameter}}" @endif>
                                            <button class="btn btn-primary " id="submit" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                            @if(isset($parameter,$filter)) <a class="btn btn-danger " href="{{url('admin/customers/display')}}"><i class="fa fa-ban" aria-hidden="true"></i> </a>@endif
                                        </div>
                                    </form>
                                    <div class="col-lg-4 form-inline" id="contact-form12"></div>
                                </div>
                                <div class="box-tools pull-right">
                                   <a href="{{ url('admin/audits/create?site='.$result['site']->id)}}" type="button" class="btn btn-block btn-primary">{{ 'Create New Audit' }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (count($errors) > 0)
                                  @if($errors->any())
                                  <div class="alert alert-success alert-dismissible" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      {{$errors->first()}}
                                  </div>
                                  @endif
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                             <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                  <tr>                      
                                    <th>{{ 'Audit' }}</th>
                                    <th>{{ 'Date' }} </th>
                                    <th width="75px">{{ 'Action' }} </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @if(count($result['audits']) > 0)
                                   @foreach ($result['audits'] as $audit)
                                      <tr>
                                          <td>{{ $audit->heading }}</td>
                                          <td>{{ $audit->created_at }}</td>
                                          <td>
                                            <a href="{{ URL::to('/admin/sites/audit',$audit->id) }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
                                          </td>
                                      </tr>
                                    @endforeach
                                  @endif
                                </tbody>
                              </table>
                              @if (count($result['audits']) > 0)
                              <div class="col-xs-12 text-right">
                                  {{$result['audits']->links()}}
                              </div>
                              @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>

        <!-- /.row -->

     

        <!-- Main row -->

        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
