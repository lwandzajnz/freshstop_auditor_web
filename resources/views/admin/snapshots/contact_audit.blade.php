@extends('admin.layout')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.Audits') }} <small>{{ trans('labels.ListingAllAudits') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Audits') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes --> 
      
            <div class="row">
            
            <div class="col-md-11">

                <div class="row">
                    <div class="col-xs-12">
                        @if (count($errors) > 0)
                          @if($errors->any())
                          <div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{$errors->first()}}
                          </div>
                          @endif
                        @endif
                    </div>
                </div>
              
                <div class="row">
                <div class="col-sm-3">
                  <div class="box">
                  <div class="box-body">
                  <p><strong>Overview</strong></p>
                  <p><strong>Failed Items</strong></p>
                  <p><strong>Audit</strong></p>
                    @foreach ($result['audit']->questions as $dat)
                      @if (strtolower($dat['question_type_two']) == 'header')
                      <p style="margin-left: 5px"><a href="#{{ $dat['question'] }}">{{ $dat['question'] }}</a></p>
                      @endif
                    @endforeach
                    </div>
                  </div>
                </div>
								<div class="col-sm-9">

                <div class="row">
                  <div class="box">
                    <div class="box-body">
                <div class="pull-right">
                 <a href="{{ URL::to('admin/snapshot/download', $result['audit']->id) }}" class="btn btn-primary">Download Report</a>
              </div>
                   
                      <img src="{{ asset('images/admin_logo/Caltex-Fresh-Stop.jpg') }}" width="300px" style="margin-bottom: -50px"/>
                      <div class="col-sm-12 text-center">
                        <h3>{{ $result['audit']->heading }}</h3>
                      </div>
                      <div class="col-sm-4">
                        <h5>Inspection Score</h5>
                        <h3>0</h3>
                      </div>
                      <div class="col-sm-4">
                        <h5>Failed Items</h5>
                        <h3>0</h3>
                      </div>
                    </div>
                    </div>
                  </div>

                <div class="row">
                  <div class="box">
                    <div class="box-body">
                      <div class="col-sm-6">
                      <h5>Site</h5>
                      <h4>{{ $result['audit']->site->site_name }}</h4>
                      </div>
                      <div class="col-sm-6">
                      <h5>Region</h5>
                      <h4>{{ $result['audit']->site->area }}</h4>
                    </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="box">
                    <div class="box-body">
                      <div class="col-sm-6">
                        <h5>Site Address</h5>
                        <h4>{{ $result['audit']->site->address }}</43>
                      </div>
                      <div class="col-sm-6">
                        <h5>Latitude | Longitude</h5>
                        <h4>{{ $result['audit']->latitude }} | {{ $result['audit']->longitude }}</43>
                      </div>
                      <div class="col-md-12">
                        <div id="map" style="width: 100%; height: 250px;"></div>
                      </div>          
                    </div>          
                  </div>
                </div>


                <div class="row">
                  <?php $valid_points = 0; ?>
                  <?php $is_header = false; ?>
                  @foreach ($result['form_data'] as $dat)
                    <div class="panel panel-default">
                    @if(strtolower($dat->question_type_two) !== 'signature' && strtolower($dat->question_type) !== 'file')
                      @if (strtolower($dat->question_type_two) == 'header')
                      <div class="panel-heading">
                        <h3 class="panel-title" id="{{ $dat->question }}">
                          {{ $dat->question  }}
                        </h3 >
                      </div>
                      @endif
                      @if (strtolower($dat->question_type_two) !== 'header')
                      <div class="panel-body">
                        <p style="margin-left: 10px">{{ $dat->question }}</p>
                        @php ($dat->answer == 'YES' && strtolower($dat->question_type_two !== 'header')) 
                              ? $value = $dat->answer .'('.$dat->question_points.')' : $value = $dat->answer;
                        @endphp
                        <h4 style="margin-left: 10px" class="label">{{ $value }}</h4>
                      </div>
                      @if(isset($dat->images) || isset($dat->notes))
                      <div class="panel-body">
                        @if(isset($dat->notes) && !empty($dat->notes))
                      <div class="form-group">
                        <h4>Notes:</h4>
                          @foreach($dat->notes as $note)
                            <p>{{ $note->note }}</p>
                          @endforeach
                        </div>
                        @endif
                        @if(isset($dat->images))
                        <div class="form-group">
                        <h4>Images:</h4>
                          @foreach($dat->images as $image)
                            <img src="{{ asset($image) }}" width="150px" />
                          @endforeach
                        </div>
                        @endif
                      </div>
                      @endif

                      @endif
                    @endif
                      
                    @if(strtolower($dat->question_type_two) == 'signature')
                    
                      <div class="panel-heading">
                        <h3>{{ $dat->answer2 }}</h3 >
                      </div>
                      <div class="panel-body">
                        <img src="{{ $dat->answer }}" />
                      </div>
                    
                    @endif
                  </div>
                  @endforeach
                    

                </div>
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbcWm1oyuFtR_euHHb8zPV9NNqYWaiFc4&v=3.exp&sensor=false"></script>
		<script>
			var map;
			function initialize() {
				// var myLatlng = new google.maps.LatLng(-33.946978, 18.649641,17);
				var myLatlng = new google.maps.LatLng("<?php echo $result['audit']->latitude;?>", "<?php echo $result['audit']->longitude;?>");
				var mapOptions = {
					zoom: 16,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					scrollwheel: false  
				};
				
				map = new google.maps.Map(document.getElementById('map'), mapOptions);
				
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'Site'
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);

		</script>

@endsection