<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
  <style>
  .page-break {
      page-break-after: always;
  }
  table {
    cellspacing: 0;
    cellpadding: 2;
    width: 700px;
  }
  table, tr, td {
    border: 0.55px solid #aaa;
  }
  td {
    padding: 8px;
    font-size: 14px;
  }
  </style>
</head>
<body>

<table style="width: 700px"  cellpadding="2" cellspacing="0">
  <tr>
    <td colspan="2"> <img src="{{ public_path('images/admin_logo/Caltex-Fresh-Stop.jpg') }}" width="250px" style=""/></td>
  </tr>
    <tr>
    <td colspan="2">
    <h3 style="margin-block-start: 0.7rem;margin-block-end: 0.7rem;">{{ $result['audit']->audit->heading }} </h3>
    <h4 style="margin-block-start: 0.7rem;margin-block-end: 0.7rem;">{{ $result['audit']->site->site_name }} / {{ $result['audit']->created_at->todatestring() }} </h4>
    <h4 style="margin-block-start: 0.7rem;margin-block-end: 0.7rem;">BC: {{ $result['audit']->user->first_name }} </h4>
    </td>
  </tr>
  <tr>
    <td width="300px"> 
      <h3>Inspection Score</h3> 
      <h4>{{ number_format($result['total_inspection_score'],2,'.','') . '%' }}</h4>
    </td>
    <td width="300px">
      <h3>Failed Items</h3>  
      <h4>{{ $result['failed_items'] }}</h4>
    </td>
  </tr>
</table>

<br/>

<table style="width: 700px" cellpadding="2" cellspacing="0">
  <tr><td><h3>Opts Manager Email Address</h3></td></tr>
  <tr><td><p>{{ $result['audit']->user->opts_manager ? $result['audit']->user->opts_manager : ' - ' }}</p></td></tr>
</table>


<br/>

<table style="width: 700px" cellpadding="2" cellspacing="0">
  <tr>
    <td width="300px">
      <h3>Site</h3>
      <p>{{ $result['audit']->site->site_name }}</p>
    </td>
    <td width="300px">
      <h3>Region</h3>
      <p>{{ $result['audit']->site->area }}</p>
    </td>
  </tr>
  <tr>
    <td width="300px">
        <h3>Site Address</h3>
        <p>{{ $result['audit']->site->address }}</p>
    </td>
    <td width="300px"> 
      <h3>Latitude | Longitude</h3>
      <p>{{ ($result['audit']->latitude > 0) ? $result['audit']->latitude : $result['audit']->end_latitude }} | {{ ($result['audit']->longitude > 0) ? $result['audit']->longitude : $result['audit']->end_longitude }}</p>
    </td>
  </tr>
</table>

<br/>

<!--
<table style="width: 700px" cellpadding="2" cellspacing="0">
  <tr>
    <td><img src="{{ asset('images/maps/map_'.$result['audit']->id.'.jpg') }}" width="250px" /></td>
  </tr>
</table>
<br/>
-->

<!--
<table style="width: 700px" cellpadding="2" cellspacing="0">
  <tr><td><h3>Failed Items</h3></td></tr>
  <tr>
    <td>
      @foreach ($result['failed_questions'] as $failed)
      <p>{{ $failed['question'] }}</p>
      @endforeach
    </td>
  </tr>
</table>
<br/>
-->

<div class="page-break"></div>

<table style="width: 700px" cellpadding="2" cellspacing="0"> 
  <?php $is_header = false; ?>
  <?php $valid_points = 0; ?>
  @foreach ($result['form_data'] as $dat)
    @if(strtolower($dat->question_type_two) !== 'signature' && strtolower($dat->question_type) !== 'file')
      @if (strtolower($dat->question_type_two) == 'header')
      <tr>
        <td colspan="3"> &nbsp; </td>
      </tr>
        <tr style="background-color: #eee; font-weight: 600">
          <td colspan="3">
              @if($dat->total_points > 0)
                {{ $dat->question . ' ('. $dat->valid_points . ' / ' . $dat->total_points .') ' }}  {{ floor($dat->valid_points / $dat->total_points * 100). '%' }} 
              @else
                0
              @endif
              </td>
        </tr>
      @endif

      @if (strtolower($dat->question_type_two) !== 'header')
       <tr>
          <td width="<?php echo in_array($result['audit']->audit->audit_type, ['contact','snapshot'])?'170px':'220px';?>">{{ $dat->question }}</td>
            @php (($dat->answer == 'YES' || $dat->answer == 1) && strtolower($dat->question_type_two !== 'header')) 
                  ? $value = $dat->answer .'('.$dat->question_points.')' : $value = $dat->answer;
            @endphp
            @php $label = '' @endphp
            @if(($dat->answer == 'YES' || $dat->answer == 1) && strtolower($dat->question_type_two !== 'header')) @php $label = '' @endphp @endif
            @if(($dat->answer == 'NO') && strtolower($dat->question_type_two !== 'header')) @php $label = 'C66565' @endphp @endif
            @if($dat->answer == 'NA' && strtolower($dat->question_type_two !== 'header')) @php $label = '' @endphp  @endif
          <td width="<?php echo in_array($result['audit']->audit->audit_type, ['contact','snapshot'])?'280px':'180px';?>" style="text-align: <?php echo in_array($result['audit']->audit->audit_type, ['contact','snapshot'])?'left':'center'?>; <?php echo ($label !== '')?'background-color: #'.$label:'';?>">{{ $value }}</td>
        
          <td width="<?php echo in_array($result['audit']->audit->audit_type, ['contact','snapshot'])?'200px':'250px';?>">
     
            @if(isset($dat->images) || isset($dat->notes))
              @if(isset($dat->notes) && !empty($dat->notes))
                  @foreach($dat->notes as $note)
                    <p>{{ $note->note }}</p>
                  @endforeach
              @endif

              
              @if(isset($dat->images))
                @foreach($dat->images as $image)
                <div style="text-align: center; display: inline">
                  <img src="{{ public_path('images/media/audit_images_resized/_'.substr($image,26)) }}" style="max-width: 80px; margin-top: 5px" width="74px" height="74px" />
                  <!-- <img src="{{ public_path('images/media/2019/10/2t7BU11909.jpg') }}" style="max-width: 80px; margin-top: 5px" width="74px" height="74px"  /> -->
                  </div>
                @endforeach
              @endif
            
            @endif
        

          </td>

        </tr>

      @endif
      
    @endif
  @endforeach
</table>
<br/>

<table style="width: 700px;" cellpadding="2px" cellspacing="0"> 
  <tr >
    <td>Name</td>
    <td>Signature</td>
  </tr>
  @foreach ($result['form_data'] as $dat)
    @if(strtolower($dat->question_type_two) == 'signature')
    <tr>
        <td width="250px"> {{ $dat->answer2 }}</td>
        <td> <img src="{{ $dat->answer }}" width="200px" /></td>
    </tr>
    
    @endif
  @endforeach                    
</table>

<div class="page-break"></div>
<table style="width: 700px" border="1" cellpadding="2" cellspacing="0"> 
  <tr>
    <td>Media</td>
  </tr>
  <tr>
    <td>
      <?php $count = 0; ?>
      @foreach ($result['form_data'] as $dat)
        @if(isset($dat->images) || isset($dat->notes))
          @if(isset($dat->images))
            @foreach($dat->images as $image)
            <div style="text-align: center; display: inline">
              <img src="{{ public_path('images/media/audit_images_resized/_'.substr($image,26)) }}" style="display: inline; margin-top: 10px" width="80px" height="80px" />
                <!-- <img src="{{ public_path('images/media/2019/10/2t7BU11909.jpg') }}" style="display: inline; margin-top: 10px" width="80px" height="80px" /> -->
              </div>
            @endforeach
          @endif
        @endif
      @endforeach                    
    </td>
  </tr>
</table>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbcWm1oyuFtR_euHHb8zPV9NNqYWaiFc4&v=3.exp&sensor=false"></script>
  <script>
    var map;
    function initialize() {
      // var myLatlng = new google.maps.LatLng(-33.946978, 18.649641,17);
      var myLatlng = new google.maps.LatLng("<?php echo ($result['audit']->latitude != 0)?$result['audit']->latitude:$result['audit']->end_latitude;?>", "<?php echo ($result['audit']->longitude != 0)?$result['audit']->longitude:$result['audit']->end_longitude;?>");
      var mapOptions = {
        zoom: 16,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false  
      };
      
      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Site'
      });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

  </script>

</body>
</html>