<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
  <style>
  .page-break {
      page-break-after: always;
  }
  </style>
</head>
<body>

<table style="width: 700px" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td colspan="2"> <img src="{{ public_path('images/admin_logo/Caltex-Fresh-Stop.jpg') }}" width="250px" style=""/></td>
    <!-- <td> <h3>{{ $result['audit']->heading }}</h3></td> -->
  </tr>
  <tr>
    <td> 
      <h4>Inspection Score</h4> 
      <h5>{{ number_format($result['total_inspection_score'],2,'.','') . '%' }}</h5>
    </td>
    <td>
      <h4>Failed Items</h4>  
      <h5>0</h5>
    </td>
  </tr>
</table>

<br/>

<table style="width: 700px" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td>
      <h4>Site</h4>
      <h5>{{ $result['audit']->site->site_name }}</h5>
    </td>
    <td>
      <h4>Region</h4>
      <h5>{{ $result['audit']->site->area }}</h5>
    </td>
  </tr>
  <tr>
    <td>
        <h4>Site Address</h4>
        <h5>{{ $result['audit']->site->address }}</h5>
    </td>
    <td> 
      <h4>Latitude | Longitude</h4>
      <h5>{{ $result['audit']->latitude }} | {{ $result['audit']->longitude }}</h5>
    </td>
  </tr>
</table>

<br/>

<table style="width: 700px" border="1" cellpadding="2" cellspacing="0"> 
  <?php $is_header = false; ?>
  <?php $valid_points = 0; ?>
  @foreach ($result['form_data'] as $dat)
    @if(strtolower($dat->question_type_two) !== 'signature' && strtolower($dat->question_type) !== 'file')
      @if (strtolower($dat->question_type_two) == 'header')
      <tr>
        <td colspan="3"> &nbsp; </td>
      </tr>
        <tr>
          <td colspan="3">{{ $dat->question . ' ('. $dat->valid_points . ' / ' . $dat->total_points .') ' }}  {{ floor($dat->valid_points / $dat->total_points * 100). '%' }} </td>
        </tr>
      @endif

      @if (strtolower($dat->question_type_two) !== 'header')
       <tr>
          <td style="width: 300px">{{ $dat->question }}</td>
            @php ($dat->answer == 'YES' && strtolower($dat->question_type_two !== 'header')) 
                  ? $value = $dat->answer .'('.$dat->question_points.')' : $value = $dat->answer;
            @endphp
            @php $label = '' @endphp
            @if($dat->answer == 'YES' && strtolower($dat->question_type_two !== 'header')) @php $label = 'success' @endphp @endif
            @if($dat->answer == 'NO' && strtolower($dat->question_type_two !== 'header')) @php $label = 'danger' @endphp @endif
            @if($dat->answer == 'NA' && strtolower($dat->question_type_two !== 'header')) @php $label = 'warning' @endphp  @endif
          <td width="100px">{{ $value }}</td>
        
          <td width="350px">
            @if(isset($dat->images) || isset($dat->notes))
              @if(isset($dat->notes) && !empty($dat->notes))
                  @foreach($dat->notes as $note)
                    <p>{{ $note->note }}</p>
                  @endforeach
              @endif

              @if(isset($dat->images))
                @foreach($dat->images as $image)
                  <img src="{{ public_path($image) }}" width="90px" />
                @endforeach
              @endif
            @endif
          </td>

        </tr>

      @endif
      
    @endif
  @endforeach
</table>
<br/>

<table style="width: 700px" border="1" cellpadding="2" cellspacing="0"> 
  <tr>
    <td>Name</td>
    <td>Signature</td>
  </tr>
  @foreach ($result['form_data'] as $dat)
    @if(strtolower($dat->question_type_two) == 'signature')
    <tr>
        <td> {{ $dat->answer2 }}</td>
        <td> <img src="{{ $dat->answer }}" /></td>
    </tr>
    
    @endif
  @endforeach                    
</table>
         
<div class="page-break"></div>
<table style="width: 700px" border="1" cellpadding="2" cellspacing="0"> 
  <tr>
    <td>Media</td>
  </tr>
  <td>
    @foreach ($result['form_data'] as $dat)
      @if(isset($dat->images) || isset($dat->notes))
        @if(isset($dat->images))
          @foreach($dat->images as $image)
            <img src="{{ asset($image) }}" width="90px" />
          @endforeach
        @endif
      @endif
    @endforeach                    
  </td>
</table>
</body>
</html>