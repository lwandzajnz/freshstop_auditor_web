{
  "enabled": false,
  "version": "1.0.1",  
  "type": "minor",
  "minor": {
    "title": "App update available",
    "message": "There's a new version availble, would you like to get it now?",
    "force": false
  },
  "major": {
    "title": "Important App Update",
    "message": "Please update your app to the latest version to continue using it...",
    "force": true
  }
}